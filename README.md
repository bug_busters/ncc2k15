# README #

This is the repo for the Bug Busters team participation in the Nokia's Code Challenge 2015

To compile and test do the usual "mvn clean install"

Code repo:
https://bitbucket.org/bug_busters/ncc2k15.git

CI page:
https://ci-bugbusters.rhcloud.com/job/mbbncc2k15/

Team page on yammer:
https://www.yammer.com/nokia.com/#/threads/inGroup?type=in_group&feedId=5513430

Scoreboard:
http://esnpmd01.emea.nsn-net.net/domjudge/public/

Judge setup instructions:
https://confluence.int.net.nokia.com/display/CCHALLENGE/Instructions+for+participants

Judge team page and clarifications:
http://esnpmd01.emea.nsn-net.net/domjudge/team/ 
