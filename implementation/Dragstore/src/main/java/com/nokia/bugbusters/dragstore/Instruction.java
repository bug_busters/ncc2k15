package com.nokia.bugbusters.dragstore;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
public class Instruction {

    private static final String GET = "get";
    private static final String SPACE = " ";
    private static final int ARRAY_FIRST_POSITION = 0;
    private final String command;
    private final long firstInstruction;
    private final long secondInstruction;

    public Instruction(String instruction) {
        String[] split = instruction.split(SPACE);
        command = split[ARRAY_FIRST_POSITION].trim();
        if (command.equalsIgnoreCase(GET)) {
            firstInstruction = Long.parseLong(split[1]);
            secondInstruction = Long.MAX_VALUE;
        } else {
            firstInstruction = Long.parseLong(split[1]);
            secondInstruction = Long.parseLong(split[3]);
        }

    }

    public Instruction(String command, Long item, Long slot) {
        this.command = command;
        this.firstInstruction = item;
        this.secondInstruction = slot;
    }

    public long getSecondInstruction() {
        return secondInstruction;
    }

    public long getFirstInstruction() {
        return firstInstruction;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash +this.command.hashCode();
        hash = 31 * hash + Float.floatToIntBits(this.firstInstruction);
        hash = 31 * hash + Float.floatToIntBits(this.secondInstruction);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Instruction other = (Instruction) obj;
        if (!this.command.equals(other.command)) {
            return false;
        }
        if (this.firstInstruction != other.firstInstruction) {
            return false;
        }
        return this.secondInstruction == other.secondInstruction;
    }

    @Override
    public String toString() {
        return "Instruction{" + "command=" + command + ", item=" + firstInstruction + ", slot=" + secondInstruction + '}';
    }

}
