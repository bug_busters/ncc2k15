package com.nokia.bugbusters.dragstore;

import java.io.InputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.TreeMap;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
public class Main {

    private static final String PILE = "pile";
    private static final String GET = "get";
    private static final String MOVE = "move";
    private static final String PUT = "put";
    private final List<Instruction> instructions = new ArrayList<>();
    private final Map<Long, Stack<Long>> slots = new TreeMap<>(new LongComparator());

    public static void main(String[] args) {
        Main main = new Main();
        main.run(System.in, System.out);
    }

    public void run(InputStream testInput, PrintStream testOutput) {
        Scanner scanner = new Scanner(testInput);
        while (scanner.hasNext()) {
            final String nextLine = scanner.nextLine();
            instructions.add(createInstruction(nextLine));
        }
        doTheMagic();
        testOutput.print(createOutput());
    }

    public Instruction createInstruction(String nextLine) {
        return new Instruction(nextLine);
    }

    private void doTheMagic() {
        for (Instruction instruction : instructions) {

            switch (instruction.getCommand()) {
                case PUT:
                    put(instruction);
                    break;
                case MOVE:

                    move(instruction);
                    break;
                case GET:

                    get(instruction);
                    break;
                case PILE:

                    pile(instruction);
                    break;

            }

        }

    }

    private void put(Instruction instruction) {
        if (!slots.containsKey(instruction.getSecondInstruction())) {
            Stack<Long> stack = new Stack<>();
            stack.add(instruction.getFirstInstruction());
            slots.put(instruction.getSecondInstruction(), stack);
        } else {
            slots.get(instruction.getSecondInstruction()).add(instruction.getFirstInstruction());
        }

    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public Map<Long, Stack<Long>> getSlots() {
        return slots;
    }

    private void move(Instruction instruction) {

        for (Stack<Long> stack : slots.values()) {
            if (!stack.empty() && stack.peek() == instruction.getFirstInstruction()) {
                Long pop = stack.pop();
                Instruction instruction1 = new Instruction(PUT, pop, instruction.getSecondInstruction());
                put(instruction1);
                break;
            }
        }

    }

    private void get(Instruction instruction) {
        for (Stack<Long> stack : slots.values()) {
            if (stack.peek() == instruction.getFirstInstruction()) {
                stack.pop();
            }
        }

    }

    private void pile(Instruction instruction) {
        final long secondInstruction = instruction.getSecondInstruction();
        final long firstInstruction = instruction.getFirstInstruction();

        if (slots.containsKey(secondInstruction)) {
            Stack<Long> donorStack = slots.get(firstInstruction);
            slots.put(firstInstruction, new Stack<Long>());

            if (slots.containsKey(firstInstruction)) {
                slots.get(secondInstruction).addAll(donorStack);
            } else {
                slots.put(firstInstruction, donorStack);
            }
        }
    }

    private String createOutput() {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Long, Stack<Long>> entrySet : slots.entrySet()) {
            Long key = entrySet.getKey();
            Stack<Long> value = entrySet.getValue();
            if (!value.empty()) {
                builder.append(key).append(":");
                for (Long value1 : value) {
                    builder.append(value1).append(" ");
                }
                builder.append("\n");
            }
        }

        return builder.toString().trim();
    }

    public static class LongComparator implements Comparator<Long>, Serializable {
        @Override
        public int compare(Long o1, Long o2) {
            return o1.compareTo(o2);
        }
    }
}
