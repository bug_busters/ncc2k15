package com.nokia.bugbusters.dragstore;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
public class MainTest {

    private static final String GLOB_IN="*.in";
    private static final String GLOB_OUT="*.out";
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream printStream = new PrintStream(outContent);

    public MainTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.setOut(printStream);
    }

    @After
    public void tearDown() {
        System.setOut(null);
    }

    /**
     * Test of main method, of class Main.
     */
    @Ignore
    @Test
    public void testMain() {
        System.out.println("test main");
        String[] args = null;
        Main.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of run method, of class Main.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testRun() throws FileNotFoundException, IOException {
//        System.out.println("test run");
        
        Main instance = new Main();
        instance.run(new FileInputStream("input"), printStream);
        
        for (Path path : Files.newDirectoryStream(Paths.get(""), GLOB_IN)) {
                System.out.println(""+path.toString());
            }
        assertEquals("1:4 5 3 6 7", outContent.toString());
        assertEquals(new String(Files.readAllBytes(Paths.get("teste1.out"))), outContent.toString());
    }

    @Test
    public void testcreateInstruction() {
        String input = "put 1 on 1";
        Main instance = new Main();
        Instruction expectedResult = new Instruction("put", 1l, 1l);

        assertEquals(expectedResult, instance.createInstruction(input));
        input = "put 1 on 2";
        assertNotSame(instance, instance.createInstruction(input));

        expectedResult = new Instruction("get", 1l, Long.MAX_VALUE);
        input = "get 1";
        System.out.println(instance.createInstruction(input));
        assertEquals(expectedResult, instance.createInstruction(input));
    }
}
