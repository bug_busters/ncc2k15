Amazing 

Problem description 

You are tired of playing games that push your brain to the limit. You have been feeling that your brain is 
starting to melt but you are so addicted that you cannot let go. For instance, four years ago you tried to 
solve your first Sudoku puzzle. You remember it well � you�ve been trying to solve it since then� 


So you think you have had enough! ! No more games! 
To make the detox easier you invented this very simple replacement game to keep you u busy and ease 
the carving feelings. 


The game consists of an NxM maze. The maze squares are numbered from 1 to N*M. Numbers do not 
repeat. You have to find the longest path with consecutive ascending numbers.


It seemed easy as a concept but now that you have started to play it you are having a h hard time solving 
the first one � you are not being able to focus and concentrate. You get to one solution n but you can�t 
trust it is the correct one � you always doubt that you are right. What started as an amazing idea is 
turning into desperation. 


So you decided to implement this p piece of software to validate your solutions and give e you some peace 
of mind. 


The input starts with the number off mazes. The following lines have the mazes with their numbered 
squares. 


As an output for each maze the maximum path length and the starting square number. If there you find 
multiple paths with the maximum p path length, choose the one with the lowest starting g square number. 
Remember that you should only at squares that are on the left, right, up or down from m the current 
square. 


Sample 

Input
3 

2x2 
3 4 
1 2 

3x3 
9 2 3 
4 5 6 
7 8 1 

2x4 
8 2 3 4 
1 6 7 5 

Output 

2 1
3 4
4 2

