module.exports=StdLine_Processor= function ( process_function , summary_function) {
	process.stdin.resume();
	process.stdin.setEncoding('utf8');
	process.stdin.on('data', function(data) {
		var lines = data.split("\n");
		for( var i=0; i <= lines.length; i++ ) {
			var line=lines[i];
			if( line || line === '' ) {
				process.stdin.emit( 'line', line );	
			}
		}
	});
	this.processor = process_function || function( line) {
		process.stdout.write(''+line+'\n');
	}
	this.summary = summary_function || function() {
		console.log( 'stdin closed');
	}
	process.stdin.on('line', this.processor);
	process.stdin.on('close', this.summary );
}

/*************
 * usage example 
//example.js

var counter=0;
function line_processor ( line) {
		counter++;
		process.stdout.write(counter+' : '+line+'\n');
	}

function summary() {
		console.log( "Counted "+counter+' lines');
	}	

var Line_Processor = require('./StdLine_Processor.js');

var runner = new Line_Processor( line_processor , summary);
 **************/