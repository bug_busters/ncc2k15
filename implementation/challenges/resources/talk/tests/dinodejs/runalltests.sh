#!/bin/sh

for input in *.jin
do
  output=`basename $input .jin`.out
  answer=`basename $input .jin`.answer
  cat $input | node talk.js > /dev/null 2> $answer
  if ! cmp $output $answer 2>/dev/null
  then
    echo $input failed
    echo "echo `cat $answer` > $output"
  fi
done
