//Talk.js

function Talk() {
  var num = function ( number) {
      if( number instanceof String ) return number;
      number %= 1000;
      var unidades= number % 10;
      var dezenas = Math.floor(number / 10) %10;
      var centenas = Math.floor(number / 100) % 10 ;
      return ''+centenas+dezenas+unidades;
  }
	var counter=0;
	var register = [];
	for( var i=0; i < 10; i++) {
	  register[i]= num(0);
	}
	var modulo = 1000;
	var RAM=[];
	var PC=0;
	var that = this;

  var printRAM = function() {
    for(var i=0; i< 1000 || i < RAM.length; i++ ) {
      if( RAM[i] && RAM[i] != '000' ) console.log( 'RAM',i, RAM[i]);
    }
  }
	this.inst1 = function ( ) {
		// halt the execution
		counter++;
		printRAM();
		console.log( register);
		console.log( 'runned ', counter, ' instructions ');
		console.error( counter);
		process.exit(0);
	}
	this.inst2 = function ( R, N) {
		// set register R equal to value N
		register[R]= num(N);
		counter++;
		PC++;
	}
	this.inst3 = function ( R, N) {
		// add value N to register R
		register[R]= num(Number(register[R])+Number(N)) ;
		counter++;
		PC++;
	}
	this.inst4 = function( R, N) {
		// multiply value N to register R
		register[R]= num( Number(register[R])*Number(N));
		counter++;
		PC++;		
	}
	this.inst5 = function( R, S) {
		// set register R to the value of S
		register[R] = num(register[S] || 0);
		counter++;
		PC++;		
	}
	this.inst6 = function( R, S) {
		// add the value of register S to register R
		register[R] = num( Number(register[R]) + Number(register[S])) ;
		counter++;
		PC++;		
	}
	this.inst7 = function( R, S) {
		//multiply register R by the value of register S
		register[R] = num( Number(register[R]) * Number(register[S]));
		counter++;
		PC++;		
	}
	this.inst8 = function( R, S) {
		//set register R to the value in RAM whose address is in register S
		register[R] = num(Number(RAM[Number(register[S])]));
		counter++;
		PC++;		
	}
	this.inst9 = function( R, S) {
		//set the value in ram whose address is in register S to the value of register R
		RAM[Number(register[S])] = num(Number(register[R]));
		console.log('RAM['+register[S]+']=',RAM[Number(register[S])])
		counter++;
		PC++;		
	}
	this.inst0 = function( R, S) {
		counter++;
		// go to the address in register R unless register S contains 0
		if( register[S] != 0 ) {
			PC = Number(register[R]);
			console.log( 'GOTO:', PC);
		} else {
			PC++;
		}
	}

	this.run = function() {
		//console.log( RAM);
		console.log( 'RUNNING ...');
		while( true ) {
		  var rampc=''+RAM[PC];
			var instruction = rampc[0] || '0';
			var arg1 = Number(rampc[1]) || 0;
			var arg2 = Number(rampc[2]) || 0;
			console.log( register, '[inst_n:'+counter+'| pc:'+PC+']', instruction, arg1, arg2, '<-',RAM[PC]);
			that['inst'+instruction]( arg1, arg2);
		}
	}
	this.load = function( line) {
		RAM.push( line);
		//console.log( 'loaded ', line);
	}
}

var talk = new Talk();	

var Line_Processor = require('./StdLine_Processor.js');
var runner = new Line_Processor( talk.load , talk.run);
