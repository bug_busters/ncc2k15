package com.nokia.bugbusters.challenges.amazing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Amazing {

	public static void main(String[] args) throws NumberFormatException, IOException {
		AmazingSolver amazingSolver = new AmazingSolver( System.in, System.out );
		amazingSolver.solve();
	}
}

class AmazingSolver {
	

	private List<Maze> mazes = new ArrayList<Maze>();
	private PrintStream outputWriter = null;
	private String output = "";

	public AmazingSolver(InputStream inputStream, OutputStream outputStream) throws NumberFormatException, IOException {
		List<String> file = AmazingSolver.readFileToArrayList(new InputStreamReader(inputStream,StandardCharsets.UTF_8));
		mazes = create_And_Return_Mazes_From_Input(file);
		outputWriter = new PrintStream(outputStream,true,"UTF-8");
	}
	
	public static List<String> readFileToArrayList(InputStreamReader fileStream) throws IOException{
		List<String> fileStrings = new ArrayList<String>();
		BufferedReader br = new BufferedReader(fileStream);
		String line = br.readLine();
		while(line != null)
		{
			fileStrings.add(line);
		    line = br.readLine();
		}
		return fileStrings;
	}

	public void solve(){
		for (int mazeNumber=0;mazeNumber < this.getMazes().size(); mazeNumber++) {
			output = output + this.getMaze(mazeNumber).solve() + System.lineSeparator();
		}
		outputWriter.print(output);
		outputWriter.close();
	}
	
	private List<Maze> create_And_Return_Mazes_From_Input(List<String> file) throws IOException {
		int index = 0;
		int readMazeIndex = 0;
		int numberOfMazes = Integer.parseInt(file.get(index).trim());
		index = index + 2;
		while (readMazeIndex < numberOfMazes) {
		    Maze maze = read_maze_from_input_with_index(index,file);
		    mazes.add(maze);
			index = index + maze.get_number_of_rows() + 2;
		    readMazeIndex++;
		}
		return mazes;
	}
	
   private Maze read_maze_from_input_with_index(int index,List<String> file) {
		String[] size_of_maze = file.get(index).split("x");
		int height = Integer.parseInt(size_of_maze[0].trim());
		int width = Integer.parseInt(size_of_maze[1].trim());
		Maze maze = new Maze(height,width);
		index++;
		int rowNumber = 0;
		for(int line = index; line < index + height; line++){
		    String[] lineValues = file.get(line).split(" ");
		    for(int columm=0; columm < lineValues.length; columm++){
		    	maze.set_cell_value(rowNumber, columm, Integer.parseInt(lineValues[columm].trim()));
		    }
		    rowNumber++;
		}
		return maze;
	}



public List<Maze> getMazes() {
		return mazes;
	}

	public Maze getMaze(int index) {
		return mazes.get(index);
	}
}

class Maze{

	private int width;
	private int height;
	private Coordinate[] squares;
	private int startingSquare;
	private int longestPath;
	

	public Maze(int _height, int _width) {
		height = _height;
		width = _width;
		squares = new Coordinate[_height*_width + 1];
		squares[0] = new Coordinate(-1,-1);
		
	}
	
	public String solve() {
		startingSquare = 1;
		longestPath = 1;
		find_longest_path_array(startingSquare,longestPath);
		return Integer.toString(longestPath) + " " + Integer.toString(startingSquare);
	}
	
	private void find_longest_path_array(int startingRoom,int longestPath) {
		 while(startingRoom + 1 <= size()){
			 int nextRoom = startingRoom + 1;
			  if(Coordinate.isAdjacent(squares[startingRoom],squares[nextRoom])){
					longestPath++;
					if(longestPath > this.longestPath){
						this.startingSquare = nextRoom - (longestPath - 1);
						this.longestPath = longestPath;
					}
			 }else{
				 longestPath = 1;
			 }
			 startingRoom++;
		 }
	 }

	public void set_cell_value(int rowNumber, int colummNumber, int value) {
		squares[value] = new Coordinate(rowNumber,colummNumber);
	}

	public int get_number_of_rows() {
		return height;
	}

	public int size() {
		return width * height;
	}
	
	public Coordinate[] getSquares(){
		return Arrays.copyOf(squares, squares.length);
	}
}

class Coordinate{

	private int row;
	private int columm;

	public Coordinate(int rowNumber, int colummNumber) {
		setRow(rowNumber);
		setColumm(colummNumber);
	}

	public boolean isAdjacent(Coordinate coordinate) {
		return row_is_adjacent(coordinate.getRow(),coordinate.getColumm()) || columm_is_adjacent(coordinate.getRow(),coordinate.getColumm());
	}
	
	public static boolean isAdjacent(Coordinate coordinate1,Coordinate coordinate2) {
		return Coordinate.row_is_adjacent(coordinate1,coordinate2) || Coordinate.columm_is_adjacent(coordinate1,coordinate2);
	}

	private static boolean row_is_adjacent(Coordinate coordinate1, Coordinate coordinate2) {
		int rowCoordinate1 = coordinate1.getRow();
		int colummCoordinate1 = coordinate1.getColumm();
		int rowCoordinate2 = coordinate2.getRow();
		int colummCoordinate2 = coordinate2.getColumm();
		
		int nextRow = rowCoordinate1 + 1;
		int previousRow = rowCoordinate1 - 1;
		if((nextRow == rowCoordinate2 || previousRow == rowCoordinate2) && colummCoordinate1 == colummCoordinate2){
			return true;
		}
		return false;
	}

	private static boolean columm_is_adjacent(Coordinate coordinate1, Coordinate coordinate2) {
		int rowCoordinate1 = coordinate1.getRow();
		int colummCoordinate1 = coordinate1.getColumm();
		int rowCoordinate2 = coordinate2.getRow();
		int colummCoordinate2 = coordinate2.getColumm();
		
		int nextColumm = colummCoordinate1 + 1;
		int previousColumm = colummCoordinate1 - 1;
		if((nextColumm == colummCoordinate2 || previousColumm == colummCoordinate2) && rowCoordinate1 == rowCoordinate2){
			return true;
		}
		return false;
	}

	private boolean columm_is_adjacent(int row,int columm) {
		int nextColumm = columm + 1;
		int previousColumm = columm - 1;
		if((nextColumm == getColumm() || previousColumm == getColumm()) && row == getRow()){
			return true;
		}
		return false;
	}

	private boolean row_is_adjacent(int row,int columm) {
		int nextRow = row + 1;
		int previousRow = row - 1;
		if((nextRow == getRow() || previousRow == getRow()) && columm == getColumm()){
			return true;
		}
		return false;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumm() {
		return columm;
	}

	public void setColumm(int columm) {
		this.columm = columm;
	}
}


