package com.nokia.bugbusters.challenges.dragstore;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
public class InstructionParameters {

    private static final String GET = "get";
    private static final String SPACE = " ";
    private static final int ARRAY_FIRST_POSITION = 0;
    private final String command;
    private final long firstArgument;
    private final long secondArgument;

    public InstructionParameters(String instruction) {
        String[] split = instruction.split(SPACE);
        command = split[ARRAY_FIRST_POSITION].trim();
        if (command.equalsIgnoreCase(GET)) {
            firstArgument = Long.parseLong(split[1]);
            secondArgument = Long.MAX_VALUE;
        } else {
            firstArgument = Long.parseLong(split[1]);
            secondArgument = Long.parseLong(split[3]);
        }

    }

    public InstructionParameters(String command, Long item, Long slot) {
        this.command = command;
        this.firstArgument = item;
        this.secondArgument = slot;
    }

    public long getSecondArgument() {
        return secondArgument;
    }

    public long getFirstArgument() {
        return firstArgument;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash +this.command.hashCode();
        hash = 31 * hash + Float.floatToIntBits(this.firstArgument);
        hash = 31 * hash + Float.floatToIntBits(this.secondArgument);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InstructionParameters other = (InstructionParameters) obj;
        if (!this.command.equals(other.command)) {
            return false;
        }
        if (this.firstArgument != other.firstArgument) {
            return false;
        }
        return this.secondArgument == other.secondArgument;
    }

    @Override
    public String toString() {
        return "Instruction{" + "command=" + command + ", item=" + firstArgument + ", slot=" + secondArgument + '}';
    }

}
