package com.nokia.bugbusters.challenges.dragstore;

import java.io.InputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.TreeMap;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
public class Main {

    private static final String ENCODING = "UTF-8";
    private static final String SPACE = " ";
    private static final String DOT = ":";
    private static final String PILE = "pile";
    private static final String GET = "get";
    private static final String MOVE = "move";
    private static final String PUT = "put";

    private final Map<Long, Stack<Long>> slots = new TreeMap<>(new LongComparator());
    private final List<Long> existingNumbers = new ArrayList<>();
    private final InputStream input;
    private final PrintStream output;

    public Main(InputStream testInput, PrintStream testOutput) {
        this.input = testInput;
        this.output = testOutput;
    }

    public static void main(String[] args) {
        Main main = new Main(System.in, System.out);
        main.solve();
    }

    public void solve() {
        try (Scanner scanner = new Scanner(input, ENCODING)) {
            while (scanner.hasNext()) {
                final String lineInput = scanner.nextLine();
                instructionExecuter(createInstruction(lineInput));
            }
            output.println(createOutput());
        }
    }

    public InstructionParameters createInstruction(String nextLine) {
        return new InstructionParameters(nextLine);
    }

    private void instructionExecuter(InstructionParameters instruction) {
        switch (instruction.getCommand()) {
            case PUT:
                put(instruction);
                break;
            case MOVE:
                move(instruction);
                break;
            case GET:
                get(instruction);
                break;
            case PILE:
                pile(instruction);
                break;
            default:
                break;
        }
    }

    private void put(InstructionParameters instruction) {
        final long destinationSlot = instruction.getSecondArgument();
        final long value = instruction.getFirstArgument();
        final boolean stackInexistent = !slots.containsKey(destinationSlot);
        boolean doesExistNumber = !existingNumbers.contains(value);

        if (doesExistNumber) {

            if (stackInexistent) {
                Stack<Long> stack = new Stack<>();
                slots.put(destinationSlot, stack);
            }
            existingNumbers.add(value);
            slots.get(destinationSlot).add(value);
        }

    }

    public Map<Long, Stack<Long>> getSlots() {
        return slots;
    }

    private void move(InstructionParameters instruction) {

        for (Stack<Long> stack : slots.values()) {
            if (!stack.empty() && stack.peek() == instruction.getFirstArgument()) {
                Long numberToMove = stack.pop();
                InstructionParameters popInstruction = new InstructionParameters(PUT, numberToMove, instruction.getSecondArgument());
                existingNumbers.remove(instruction.getFirstArgument());
                put(popInstruction);
                break;
            }
        }

    }

    private void get(InstructionParameters instruction) {
        for (Stack<Long> stack : slots.values()) {
            if (!stack.empty() && stack.peek() == instruction.getFirstArgument()) {
                stack.pop();
                existingNumbers.remove(instruction.getFirstArgument());
            }
        }

    }

    private void pile(InstructionParameters instruction) {
        final long receiverPile = instruction.getSecondArgument();
        final long donorPile = instruction.getFirstArgument();
        final boolean stackExists = slots.containsKey(receiverPile);

        Stack<Long> donorStack = slots.get(donorPile);
        final boolean donorPileExists = slots.containsKey(donorPile);
        if (stackExists) {

            if (donorPileExists) {
                slots.get(receiverPile).addAll(donorStack);
            }

            donorStack.clear();
        } else {
            if (donorPileExists) {
                Stack<Long> newPile = new Stack<>();
                newPile.addAll(donorStack);
                slots.put(receiverPile, newPile);
            }
            donorStack.clear();
        }
    }

    private String createOutput() {
        StringBuilder outputBuilder = new StringBuilder();
        for (Map.Entry<Long, Stack<Long>> entrySet : slots.entrySet()) {
            Long key = entrySet.getKey();
            Stack<Long> stack = entrySet.getValue();
            final boolean stackNotEmpty = !stack.empty();

            if (stackNotEmpty) {
                outputBuilder.append(key).append(DOT).append(SPACE);
                for (Long valueInStack : stack) {
                    outputBuilder.append(valueInStack).append(SPACE);
                }
                outputBuilder.deleteCharAt(outputBuilder.length() - 1);
                outputBuilder.append(System.lineSeparator());
            }
        }

        return outputBuilder.toString().trim();
    }

    public static class LongComparator implements Comparator<Long>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(Long o1, Long o2) {
            return o1.compareTo(o2);
        }
    }
}
