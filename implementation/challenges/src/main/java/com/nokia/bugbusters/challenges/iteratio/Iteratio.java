package com.nokia.bugbusters.challenges.iteratio;


import java.io.*;
import java.util.*;

/**
 * Created by Dinis on 04-04-2015.
 */


class IterationsForNumber implements Comparable {
    private int number;
    private int iterations;

    private static final int maximumLimit = 5000001;
    private static int[] cache = new int[maximumLimit];

    IterationsForNumber(int inputNumber) {
        this.number = inputNumber;
        this.iterations = IterationsForNumber.calculateIterationsForNumberCached(inputNumber);
    }

    @Override
    public int compareTo( Object otherObject) {
        IterationsForNumber other = (IterationsForNumber) otherObject;
        return this.iterations - other.iterations;
    }
    public int getNumber() { return this.number; }
    public int getIterations() { return this.iterations;}

    public String toString() {
        return this.iterations+" "+this.number;
    }

    static public boolean isOdd( long number) {
        return ( (number % 2) != 0);
    }

    private static int calculateIterationsForNumberCached( int inputNumber) {
        int iterations = 0;
        long cycleNumber= inputNumber;
        while (true) {
            if ( cycleNumber < maximumLimit && cache[(int) (cycleNumber)] != 0 ) {
                iterations += cache[(int)cycleNumber];
                break;
            }
            iterations++;
            if (cycleNumber == 1) {
                break;
            }
            if ( isOdd(cycleNumber)) {
                cycleNumber = cycleNumber * 3 + 1;
            } else {
                cycleNumber /= 2;
            }
        }
        if ( inputNumber < maximumLimit ) {
            cache[inputNumber] = iterations;
        }
        return iterations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IterationsForNumber that = (IterationsForNumber) o;

        if (iterations != that.iterations) {
            return false;
        }
        return (number != that.number);
    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + iterations;
        return result;
    }
}


class MaximumIterationsForInterval {
    private List biggersSoFar = null;
    private void setBigger( IterationsForNumber other) {
        if ( this.biggersSoFar == null
                || ((IterationsForNumber) this.biggersSoFar.get(0)).compareTo(other) < 0 ){
            this.biggersSoFar = new ArrayList();
            this.biggersSoFar.add( other);
            return;
        }
        if (((IterationsForNumber) this.biggersSoFar.get(0)).compareTo(other) == 0 ) {
            this.biggersSoFar.add(other);
        }
    }
    public MaximumIterationsForInterval( int start, int end) {
        for( int i = start; i <= end; i++) {
            this.setBigger(new IterationsForNumber(i));
        }
    }
    public String toString() {
        if (this.biggersSoFar == null ) {
            return "none";
        }
        StringBuilder solution = new StringBuilder(""+ ((IterationsForNumber) this.biggersSoFar.get(0)).getIterations());
        Iterator it = this.biggersSoFar.iterator();
        while (it.hasNext()) {
            IterationsForNumber el = (IterationsForNumber) it.next();
            solution.append(" ").append(el.getNumber());
        }
        return solution.toString();
    }
}

class IteratioSolver{

    private final InputStream input;
    private final PrintStream output;

    IteratioSolver( InputStream input, PrintStream output ) throws IOException {
        this.input = input;
        this.output = output;
    }

    public void solve() {
        Scanner lineScanner = new Scanner( this.input, "utf-8" );
        if ( lineScanner.hasNext() ) {
            int numberOfIntervals = Integer.parseInt(lineScanner.nextLine(),10);
            for( int interval=1; interval <= numberOfIntervals; interval++ ) {
                if ( lineScanner.hasNext() ){
                    String lineWithInterval = (String) lineScanner.nextLine();
                    String[] boundaries = lineWithInterval.split(" ");
                    if( boundaries.length == 2 ) {
                        int start = Integer.parseInt(boundaries[0], 10);
                        int end = Integer.parseInt(boundaries[1], 10);
                        this.output.println( ""+ (new MaximumIterationsForInterval( start,end)).toString());
                    }
                }
            }
        }

    }


}

public class Iteratio {


    public static void main( String args[] ) throws IOException {
        IteratioSolver iteratioSolver = new IteratioSolver( System.in, System.out );
        iteratioSolver.solve();
    }

}
