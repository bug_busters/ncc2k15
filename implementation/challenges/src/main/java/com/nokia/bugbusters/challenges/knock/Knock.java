package com.nokia.bugbusters.challenges.knock;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by pt103441 on 04-04-2015.
 */

public class Knock {

    public static void main(String args[]) throws IOException {
    	KnockSolver knockSolver = new KnockSolver(System.in, System.out);
        knockSolver.solve();
    }

}


class KnockSolver{
	
	private List<Problem> problems = new ArrayList<Problem>();
	private BufferedReader inputReader = null;
	private PrintStream outputWriter = null;
	private String output = "";
	
	public KnockSolver(InputStream inputStream, OutputStream outputStream) throws IOException {
	    	inputReader = new BufferedReader(new InputStreamReader(inputStream,StandardCharsets.UTF_8));
	    	outputWriter = new PrintStream(outputStream,true,"UTF-8");
	    	problems = create_Problems_From_Input();
	    }
	
	public void solve(){
		for (int problemNumber=0;problemNumber < this.getProblems().size(); problemNumber++) {
			output = output + this.getProblem(problemNumber).solve()  + System.lineSeparator();
		}
		outputWriter.print(output);
		outputWriter.close();
	}
	
	private List<Problem> create_Problems_From_Input() throws NumberFormatException, IOException {
		int numberOfProblems = read_number_of_problems();
		int problemIndex = 0;
		while (problemIndex < numberOfProblems) {
		    Problem problem = read_problem();
		    problems.add(problem);
		    problemIndex++;
		}
		return problems;
	}
	
	private int read_number_of_problems() throws NumberFormatException, IOException {
		String line;
		int numberOfProblems = 0;
		while ((line = inputReader.readLine()) != null) {
		    if(is_a_number(line)){
		    	numberOfProblems = Integer.parseInt(line.trim());
		    	break;
		    }
		}
		return numberOfProblems;
	}
	
	private Problem read_problem() throws NumberFormatException, IOException {
		Problem problem = new Problem();
		read_size_of_problem(problem);
		read_doors_for_problem(problem);
		return problem;
	}
	
	private Problem read_size_of_problem(Problem problem) throws NumberFormatException, IOException {
		String line;
		while ((line = inputReader.readLine()) != null) {
		    if(is_the_size_of_problem(line)){
		    	String[] size_of_problem = line.trim().split(" ");
	    		problem.setHeight(Integer.parseInt(size_of_problem[0]));
	    		problem.setWidth(Integer.parseInt(size_of_problem[1]));
		    	break;
		    }
		}
		return problem;
	}
	
	private boolean is_the_size_of_problem(String line) {
		line = line.trim();
		return line.matches("\\d+ \\d+");
	}

	private void read_doors_for_problem( Problem problem) throws NumberFormatException, IOException {
		String line;
		int maxNumberOfRows = problem.getHeight();
		int maxNumberOfColumns = problem.getWidth();
		int numberOfRoomToRead = maxNumberOfRows * maxNumberOfColumns;
		int currentRow = 0;
		int maxColummIndex =  maxNumberOfColumns - 1;
		int currentColumm = maxColummIndex;
		int roomCounter = 1;
	   
		while ((line = inputReader.readLine()) != null) {
		    String[] doors = line.trim().split(" ");
			Coordinate currentCoordinate = new Coordinate(currentRow,currentColumm);
		    problem.create_room(doors[0],currentCoordinate.getUpCoordinate());
		    problem.create_room(doors[1],currentCoordinate.getRightCoordinate(maxNumberOfColumns));
		    problem.create_room(doors[2],currentCoordinate.getLeftCoordinate());
		    problem.create_room(doors[3],currentCoordinate.getDownCoordinate(maxNumberOfRows));
		    
		    boolean starting_reading_new_row = roomCounter % maxNumberOfColumns == 0;
			if(starting_reading_new_row){
		    	currentRow++;
		    	currentColumm = maxColummIndex;
		    }else{
		    	currentColumm--;
		    }
		    if(roomCounter == numberOfRoomToRead){
		    	break;
		    }
		    roomCounter++;
		}
		
		if(problem.getSize() == 1){
			problem.getRooms().put(1, new Coordinate(0,0));
		}
		
	}
	
	private boolean is_a_number(String line) {
		line = line.trim();
		try{
			Integer.parseInt(line);
		}catch(NumberFormatException expection){
			return false;
		}
		return true;
	}
	
	public Problem getProblem(int index) {
		return problems.get(index);
	}
	
	public List<Problem> getProblems() {
		return problems;
	}
	
}

class Problem {
	private Map<Integer,Coordinate> rooms = new TreeMap<Integer,Coordinate>();
	private int height;
	private int width;
	private int startingSquare;
	private int longestPath;

	public int getSize() {
		return height * width;
	}

	public void setWidth(int _with) {
		width = _with;
	}

	public void setHeight(int _height) {
		height = _height;
	}

	public Problem() {
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public String solve() {
    	startingSquare = 1;
		longestPath = 1;
		find_longest_path(startingSquare,longestPath);
		return  Integer.toString(longestPath) + " " + Integer.toString(startingSquare);
    }
    
	 private void find_longest_path(int startingRoom,int longestPath) {
		 while(startingRoom + 1 <= getSize()){
			 int nextRoom = startingRoom + 1;
			 if(rooms.get(nextRoom).isAdjacent(rooms.get(startingRoom))){
					longestPath++;
					if(longestPath > this.longestPath){
						this.startingSquare = nextRoom - (longestPath - 1);
						this.longestPath = longestPath;
					}
			 }else{
				 longestPath = 1;
			 }
			 startingRoom++;
		 }
	 }
    
    public void create_room(String _doorValue,Coordinate coordinate) {
		Integer doorValue = getDoorNumberFromString(_doorValue);
		boolean doorHaveNumber = doorValue != null;
		if(doorHaveNumber){
			boolean roomAlreadyExist = rooms.containsKey(doorValue);
			if(! roomAlreadyExist) {
				createNewRoom(coordinate, doorValue.intValue());
			}
		}
	}
	
	private Integer getDoorNumberFromString(String _doorValue) {
		Integer doorValue = null;
		try{
			doorValue = Integer.parseInt(_doorValue);
			return doorValue;
		}catch(NumberFormatException expection){
			return doorValue;
		}
	}
	
	private void createNewRoom(Coordinate coordinate, int roomValue) {
		rooms.put(roomValue, coordinate);
	}

	public Map<Integer,Coordinate> getRooms() {
		return rooms;
	}
}

class Coordinate{

	private int row;
	private int columm;

	public Coordinate(int rowNumber, int colummNumber) {
		setRow(rowNumber);
		setColumm(colummNumber);
	}

	public Coordinate getRightCoordinate(int numberMaxOfColumns) {
		int nextColumm = columm + 1;
		if(nextColumm < numberMaxOfColumns)
		{
			return new Coordinate(row,nextColumm);
		}
		return null;
	}

	public Coordinate getLeftCoordinate() {
		int previousColumm = columm - 1;
		if(previousColumm >= 0 )
		{
			return new Coordinate(row,previousColumm);
		}
		return null;
	}

	public Coordinate getDownCoordinate(int numberMaxOfRows) {
		int nextRow = row + 1;
		if(nextRow < numberMaxOfRows)
		{
			return new Coordinate(nextRow,columm);
		}
		return null;
	}

	public Coordinate getUpCoordinate() {
		int previousRow = row -1;
		if(previousRow >= 0)
		{
			return new Coordinate(previousRow,columm);
		}
		return null;
	}

	public boolean isAdjacent(Coordinate coordinate) {
		return row_is_adjacent(coordinate.getRow(),coordinate.getColumm()) || columm_is_adjacent(coordinate.getRow(),coordinate.getColumm());
	}

	private boolean columm_is_adjacent(int row,int columm) {
		int nextColumm = columm + 1;
		int previousColumm = columm - 1;
		if((nextColumm == getColumm() || previousColumm == getColumm()) && row == getRow()){
			return true;
		}
		return false;
	}

	private boolean row_is_adjacent(int row,int columm) {
		int nextRow = row + 1;
		int previousRow = row - 1;
		if((nextRow == getRow() || previousRow == getRow()) && columm == getColumm()){
			return true;
		}
		return false;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumm() {
		return columm;
	}

	public void setColumm(int columm) {
		this.columm = columm;
	}
	
}
