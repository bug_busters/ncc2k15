package com.nokia.bugbusters.challenges.magic;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by dinmarti on 04-04-2015.
 */
class MagicSolver {

    private final InputStream input;
    private final PrintStream output;

    MagicSolver(InputStream input, PrintStream output) throws IOException {
        this.input = input;
        this.output = output;
    }

    public void solve() {
        int numberOfCards = 0;
        int numberOfCases = 0;
        String magicianName = "";
        String helperName = "";

        Scanner lineScanner = new Scanner(this.input, "utf-8");
        if (lineScanner.hasNextLine()) {
            numberOfCards = Integer.parseInt(lineScanner.nextLine());
        }
        if (lineScanner.hasNextLine()) {
            numberOfCases = Integer.parseInt(lineScanner.nextLine());
        }
        if (lineScanner.hasNextLine()) {
            magicianName = lineScanner.nextLine();
        }
        if (lineScanner.hasNextLine()) {
            helperName = lineScanner.nextLine();
        }
        for (int caseN = 0; caseN < numberOfCases; caseN++) {
            CardGuessGame game = null;
            if (lineScanner.hasNextLine()) {
                game = new CardGuessGame(lineScanner.nextLine(), numberOfCards);
                game.setMagicianName(magicianName);
                game.setHelperPlayerName(helperName);
            }
            if (game != null) {
                for (int table = 0; table < 2; table++) {
                    for (int row = 0; row < game.getDimension(); row++) {
                        if (lineScanner.hasNextLine()) {
                            game.appendRowOf(table, lineScanner.nextLine());
                        } else {
                            break;
                        }
                    }
                    if (lineScanner.hasNextLine()) {
                        game.setAnswerOf(table, Integer.parseInt(lineScanner.nextLine()));
                    }
                }
                if (lineScanner.hasNextLine()) {
                    game.setCardGuessed(Integer.parseInt(lineScanner.nextLine()));
                }
                if (lineScanner.hasNextLine()) {
                    game.setHelperFeedBack(lineScanner.nextLine().equalsIgnoreCase("yes"));
                }
                this.output.println(game.toString());
            }

        }

    }
}

public class Magic {

    public static void main(String args[]) throws IOException {
        MagicSolver MagicSolver = new MagicSolver(System.in, System.out);
        MagicSolver.solve();
    }

}

class CardGameException extends Exception {

    public CardGameException(String str) {
        super(str);
    }
}

class CardGuessSquareMatrix {

    private int[][] matrix;
    private int cursor = 0;

    public CardGuessSquareMatrix(int dimension) {
        this.matrix = new int[dimension][dimension];
    }

    public void appendRow(String line) throws CardGameException {
        if (cursor >= this.matrix.length) {
            throw new CardGameException("Row out of bounds");
        }
        String[] cards = line.split(" ");
        for (int index = 0; index < cards.length && index < this.matrix.length; index++) {
            if (index > this.matrix.length) {
                throw new CardGameException("Column out of bounds");
            }
            this.matrix[cursor][index] = Integer.parseInt(cards[index]);
        }
        cursor++;
    }

    public int[] getRow(int row) throws CardGameException {

        if (--row >= this.matrix.length) {
            throw new CardGameException("Row out of bounds");
        }
        return this.matrix[row];
    }

    public List<Integer> getRowAsList(int row) {
        List<Integer> rowList = new ArrayList<>();
        try {
            for (int card : this.getRow(row)) {
                rowList.add(card);
            }
        } finally {
            return rowList;
        }
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (int row = 0; row < this.matrix.length; row++) {
            for (int column = 0; column < this.matrix[row].length; column++) {
                out.append(this.matrix[row][column]).append(" ");
            }
            out.append(System.lineSeparator());
        }
        return out.toString();
    }

    public int getDimension() {
        return this.matrix.length;
    }
}

class CardGuessGame {

    private String label;
    private int numberOfCards;
    private String magicianName;
    private String helperPlayerName;
    private CardGuessSquareMatrix[] table;
    private int[] answer;
    private int cardGuessed;
    private List<Integer> possibleCards;
    private boolean helperFeedBack;

    public CardGuessGame(String label, int numberOfCards) {
        this.label = label;
        this.numberOfCards = numberOfCards;
        this.table = new CardGuessSquareMatrix[2];
        this.answer = new int[2];
        for (int index = 0; index < 2; index++) {
            this.table[index] = new CardGuessSquareMatrix(this.getDimension());
            this.answer[index] = 0;
        }
        this.cardGuessed = 0;
        this.possibleCards = new ArrayList<>();
    }

    public void setMagicianName(String name) {
        this.magicianName = name;
    }

    public void setHelperPlayerName(String name) {
        this.helperPlayerName = name;
    }

    public final int getDimension() {
        return calculateMatrixDimension(this.numberOfCards);
    }

    private static int calculateMatrixDimension(int numberOfCards) {
        return (int) Math.ceil(Math.sqrt((double) numberOfCards));
    }

    public boolean isGoodMagician() {
        if (this.possibleCards.isEmpty()) {
            this.possibleCards = this.table[0].getRowAsList(this.answer[0]);
            this.possibleCards.retainAll(this.table[1].getRowAsList(this.answer[1]));
        }
        List cardsToDisperse = this.table[0].getRowAsList(this.answer[0]);
        for (int newRow = 1; newRow <= this.table[1].getDimension(); newRow++) {
            boolean selectedRowChanged = cardsToDisperse.removeAll(this.table[1].getRowAsList(newRow));
            if (!selectedRowChanged) {
                return false;
            }
        }
        if (!cardsToDisperse.isEmpty()) {
            return false;
        }

        return (this.possibleCards.size() == 1 && this.cardGuessed == this.possibleCards.get(0));
    }

    public boolean isHelperHonest() {
        if (this.isGoodMagician()) {
            return this.helperFeedBack;
        } else {
            final boolean magicFailedTheRow = this.possibleCards.isEmpty();
            if (magicFailedTheRow || this.possibleCards.size() >= 1 && !this.possibleCards.contains(this.cardGuessed)) {
                return !this.helperFeedBack;
            } else {
                return true;
            }
        }
    }

    public void appendRowOf(int i, String s) {
        try {
            this.table[i].appendRow(s);
        } catch (CardGameException e) {
        }
    }

    public void setAnswerOf(int tableN, int row) {
        this.answer[tableN] = row;
    }

    public void setCardGuessed(int card) {
        this.cardGuessed = card;
    }

    public void setHelperFeedBack(boolean feedBack) {
        this.helperFeedBack = feedBack;
    }

    public String toString() {
        String out = this.label + ": ";
        out += this.magicianName + " is a ";
        if (this.isGoodMagician()) {
            out += "good";
        } else {
            out += "bad";
        }
        out += " magician and ";
        out += this.helperPlayerName + " was ";
        if (this.isHelperHonest()) {
            out += "honest";
        } else {
            out += "dishonest";
        }

        return out;
    }
}
