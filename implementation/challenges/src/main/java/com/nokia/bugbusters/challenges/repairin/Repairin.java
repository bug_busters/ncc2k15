package com.nokia.bugbusters.challenges.repairin;

import java.io.*;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by pt103441 on 04-04-2015.
 */

class Point implements Comparable<Point> {

    long x, y;

    private int hashCode;

    Point(long x, long y) {
        this.x = x;
        this.y = y;

        int result = (int) (x ^ (x >>> 32));
        hashCode = 31 * result + (int) (y ^ (y >>> 32));
    }


    public int compareTo(Point other) {
        int compareResult;

        if (this.x == other.x) {
            //compareResult = Long.valueOf ( this.y - other.y ).intValue() ;
            compareResult = Long.compare( this.y , other.y );
        } else {
            //compareResult = Long.valueOf( this.x - other.x ).intValue();
            compareResult = Long.compare( this.x , other.x );
        }

        return compareResult;
    }

    public String toString() {
        return "" + x + "," + y + "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Point point = (Point) o;

        if (x != point.x || y != point.y ) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }
}

class RepairinSolver {

    public static boolean crossLessOrEqualZero(Point O, Point A, Point B) {

        double part1 = ( A.x - O.x ) ;
        double part2 = ( B.y - O.y ) ;
        double part3 = ( A.y - O.y ) ;
        double part4 = ( B.x - O.x ) ;
        double answer = part1 * part2 - part3 * part4 ;
        boolean retval = answer <= 0.0;

        //System.out.println("(" + A.x + " - " + O.x + ") * ( " + B.y + " - " + O.y + "  ) - ( " + A.y + " - " + O.y + " ) * ( " + B.x + " - " + O.x + ") == " + answer);

        return retval;

    }

    public static Point[] convex_hull(Point[] P) {

        if (P.length > 1) {
            int n = P.length, k = 0;
            Point[] H = new Point[2 * n];


            Arrays.sort(P);

            // Build lower hull
            for (int i = 0; i < n; ++i) {
                while (k >= 2 && crossLessOrEqualZero(H[k - 2], H[k - 1], P[i]) ) {
                    k--;
                }
                H[k++] = P[i];
            }

            // Build upper hull
            for (int i = n - 2, t = k + 1; i >= 0; i--) {
                while (k >= t && crossLessOrEqualZero(H[k - 2], H[k - 1], P[i])) {
                    k--;
                }
                H[k++] = P[i];
            }
            if (k > 1) {
                H = Arrays.copyOfRange(H, 0, k - 1);
            }
            return H;
        } else {
            return P;
        }
    }

    private static long nextLong(StreamTokenizer tokenizer) throws IOException {
        return Double.valueOf(nextDouble(tokenizer)).longValue();
    }

    private static int nextInt(StreamTokenizer tokenizer) throws IOException {
        return Double.valueOf(nextDouble(tokenizer)).intValue();
    }

    private static double nextDouble(StreamTokenizer tokenizer) throws IOException {
        while (true) {
            if (tokenizer.nextToken() == StreamTokenizer.TT_EOF || tokenizer.ttype == StreamTokenizer.TT_NUMBER) {
                return tokenizer.nval;
            }
        }
    }




    public static void solve(InputStream input, PrintStream output) throws IOException {

        Point[] points = readPointsWithStreamTokenizer(input);
        Point convexHull[] = convex_hull(points);
        
        outputWithStringBuilder(output, convexHull);

    }

    public static Point[] readPointsWithStreamTokenizer(InputStream input) throws IOException {
        StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(input, "UTF-8")));
        int nrOfPoints = nextInt(tokenizer);
        Point points[] = new Point[nrOfPoints];

        for (int i = 0; i < nrOfPoints; i++) {
            points[i] = new Point(nextLong(tokenizer), nextLong(tokenizer));
        }
        return points;
    }

    public static void outputWithStringBuilder(PrintStream output, Point[] convexHull) throws IOException {
        int indexOfPointWithLowestCoordinates = indexOfPointWithLowestCoordinates(convexHull);

        StringBuilder strOutput = new StringBuilder();
/*
        for( Point point: convexHull){
            System.out.println( point );
        }
*/
        int currIdx = indexOfPointWithLowestCoordinates;
        int counter = 0;
        int pointCounter = 0;
        Point lastPoint = null;
        while( counter < convexHull.length ){
            Point currPoint = convexHull[  currIdx ];
            if( ! currPoint.equals( lastPoint ) ) {
                strOutput.append(currPoint + System.lineSeparator());
                pointCounter++;
            }
            currIdx--;
            if(currIdx < 0){
                currIdx = convexHull.length -1;
            }
            counter++;
            lastPoint = currPoint;

        }

        output.write( String.valueOf( pointCounter + System.lineSeparator() + strOutput ).getBytes("UTF-8"));
    }


    private static int indexOfPointWithLowestCoordinates(Point[] pointArray) {
        int lowestCoordinatesPointIndex = 0;
        long lowestXCoordinate = pointArray[0].x;
        long lowestYCoordinate = pointArray[0].y;
        for (int i = 1; i < pointArray.length; i++) {
            if (pointArray[i].x == lowestXCoordinate && pointArray[i].y < lowestYCoordinate) {
                lowestYCoordinate = pointArray[i].y;
                lowestCoordinatesPointIndex = i;
            }
            if (pointArray[i].x < lowestXCoordinate) {
                lowestXCoordinate = pointArray[i].x;
                lowestCoordinatesPointIndex = i;
            }
        }
        return lowestCoordinatesPointIndex;
    }

}

public class Repairin {

    public static void main( String args[] ) throws IOException {

        RepairinSolver.solve(System.in, System.out);
    }

}
