package com.nokia.bugbusters.challenges.repairin_br;

import java.io.*;
import java.util.*;

/**
 * Created by pt103441 on 04-04-2015.
 */

class Point implements Comparable<Point> {

    long x, y;

    private int hashCode;

    Point(long x, long y) {
        this.x = x;
        this.y = y;

        int result = (int) (x ^ (x >>> 32));
        hashCode = 31 * result + (int) (y ^ (y >>> 32));
    }


    public int compareTo(Point other) {
        int compareResult;

        if (this.x == other.x) {
            compareResult = Long.valueOf ( this.y - other.y ).intValue() ;
        } else {
            compareResult = Long.valueOf( this.x - other.x ).intValue();
        }

        return compareResult;
    }

    public String toString() {
        return "" + x + "," + y + "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Point point = (Point) o;

        if (x != point.x || y != point.y ) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }
}

class RepairinSolver {

    public static long cross(Point O, Point A, Point B) {
        return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
    }

    public static Point[] convex_hull(Point[] P) {

        if (P.length > 1) {
            int n = P.length, k = 0;
            Point[] H = new Point[2 * n];


            Arrays.sort(P);

            // Build lower hull
            for (int i = 0; i < n; ++i) {
                while (k >= 2 && cross(H[k - 2], H[k - 1], P[i]) <= 0) {
                    k--;
                }
                H[k++] = P[i];
            }

            // Build upper hull
            for (int i = n - 2, t = k + 1; i >= 0; i--) {
                while (k >= t && cross(H[k - 2], H[k - 1], P[i]) <= 0) {
                    k--;
                }
                H[k++] = P[i];
            }
            if (k > 1) {
                H = Arrays.copyOfRange(H, 0, k - 1);
            }
            return H;
        } else {
            return P;
        }
    }

    public static List<String> readFileToArrayList(InputStream input) throws IOException{
        List<String> fileStrings = new ArrayList<String>();
        BufferedReader br = new BufferedReader( new InputStreamReader(input, "UTF-8") );
        String line = br.readLine();
        while(line != null)
        {
            fileStrings.add(line);
            line = br.readLine();
        }
        return fileStrings;
    }

    public static void solve(InputStream input, PrintStream output) throws IOException {

        long checkpoints[] = new long[ 6 ];

        checkpoints[0] = new Date().getTime();

        Point[] points = readPointsWithBufferedReader(input);

        checkpoints[1] = new Date().getTime();

        Point convexHull[] = convex_hull(points);

        checkpoints[2] = new Date().getTime();

        Set<Point> pointSet = new HashSet<>(Arrays.asList(convexHull));
        convexHull = pointSet.toArray( new Point[pointSet.size()] );

        checkpoints[3] = new Date().getTime();

        if (convexHull.length > 1) {
            Arrays.sort(convexHull, new Comparator<Point>() {
                @Override
                public int compare(Point o1, Point o2) {
                    return -Long.valueOf((o2.x * o1.y) - (o1.x * o2.y)).intValue();
                }
            });
        }

        checkpoints[4] = new Date().getTime();

        outputWithStringBuilder(output, convexHull);

        checkpoints[5] = new Date().getTime();

        for( int i = 1  ; i < checkpoints.length ; i++ ){
            System.out.println( i + ": " + ( checkpoints[i] - checkpoints[0] ) + " - " + ( checkpoints[i] - checkpoints[i-1] ) );
        }


    }

    public static Point[] readPointsWithBufferedReader(InputStream input) throws IOException {
        List<String> inputLines = readFileToArrayList( input );

        int nrOfPoints = Integer.parseInt(inputLines.get( 0 ).trim());
        Point points[] = new Point[nrOfPoints];

        ;
        for (int i = 0; i < nrOfPoints; i++) {
            String coordinates[] = inputLines.get( i + 1 ).split(",");
            points[i] = new Point( Long.parseLong( coordinates[0].trim() ),  Long.parseLong( coordinates[1].trim() ) );
        }
        return points;
    }

    public static void outputWithStringBuilder(PrintStream output, Point[] convexHull) throws IOException {
        int indexOfPointWithLowestCoordinates = indexOfPointWithLowestCoordinates(convexHull);

        StringBuilder strOutput = new StringBuilder();

        strOutput.append( convexHull.length + System.lineSeparator() );
        for (int i = indexOfPointWithLowestCoordinates; i < convexHull.length + indexOfPointWithLowestCoordinates; i++) {
            strOutput.append(convexHull[i % convexHull.length] + System.lineSeparator() );
        }

        output.write( String.valueOf( strOutput ).getBytes("UTF-8"));
    }


    private static int indexOfPointWithLowestCoordinates(Point[] pointArray) {
        int lowestCoordinatesPointIndex = 0;
        long lowestXCoordinate = pointArray[0].x;
        long lowestYCoordinate = pointArray[0].y;
        for (int i = 1; i < pointArray.length; i++) {
            if (pointArray[i].x == lowestXCoordinate && pointArray[i].y < lowestYCoordinate) {
                lowestYCoordinate = pointArray[i].y;
                lowestCoordinatesPointIndex = i;
            }
            if (pointArray[i].x < lowestXCoordinate) {
                lowestXCoordinate = pointArray[i].x;
                lowestCoordinatesPointIndex = i;
            }
        }
        return lowestCoordinatesPointIndex;
    }

}

public class Repairin {

    public static void main( String args[] ) throws IOException {
        RepairinSolver.solve( System.in , System.out  );
    }

}
