package com.nokia.bugbusters.challenges.robot;

import java.util.Objects;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
class Event {

    private static final int EVENT_ID_POSITION = 0;
    private static final int ROBOT_NAME_POSITION = 1;
    private static final int RADIATION_POSITION = 2;
    private static final int X_POSITION = 3;
    private static final int Y_POSITION = 4;

    private final String robotName;
    private final String rawEvent;
    private final RadiationLevels radiationLevel;
    private long eventId;
    private long radiationLevelValue;
    private long positionX;
    private long positionY;

    public Event(String rawEvent) {
        String[] splittedEvent = rawEvent.split(" ");
        this.rawEvent = rawEvent;
        this.eventId = Long.parseLong(splittedEvent[EVENT_ID_POSITION]);
        this.radiationLevelValue = Long.parseLong(splittedEvent[RADIATION_POSITION]);
        this.positionX = Long.parseLong(splittedEvent[X_POSITION]);
        this.positionY = Long.parseLong(splittedEvent[Y_POSITION]);
        this.robotName = splittedEvent[ROBOT_NAME_POSITION];
        this.radiationLevel=RadiationLevels.getRadiationByLevel(radiationLevelValue);
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public long getRadiationLevelLongValue() {
        return radiationLevelValue;
    }

    public void setRadiationLevelLongValue(long radiationLevel) {
        this.radiationLevelValue = radiationLevel;
    }

    public long getPositionX() {
        return positionX;
    }

    public void setPositionX(long positionX) {
        this.positionX = positionX;
    }

    public long getPositionY() {
        return positionY;
    }

    public void setPositionY(long positionY) {
        this.positionY = positionY;
    }

    public String getRobotName() {
        return robotName;
    }

    public String getRawEvent() {
        return rawEvent;
    }

    public RadiationLevels getRadiationLevel() {
        return radiationLevel;
    }
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.robotName);
        hash = 83 * hash + (int) (this.eventId ^ (this.eventId >>> 32));
        hash = 83 * hash + (int) (this.radiationLevelValue ^ (this.radiationLevelValue >>> 32));
        hash = 83 * hash + (int) (this.positionX ^ (this.positionX >>> 32));
        hash = 83 * hash + (int) (this.positionY ^ (this.positionY >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (!Objects.equals(this.robotName, other.robotName)) {
            return false;
        }
        if (this.radiationLevelValue != other.radiationLevelValue) {
            return false;
        }
        if (this.positionX != other.positionX) {
            return false;
        }
        return this.positionY == other.positionY;
    }
    
}
