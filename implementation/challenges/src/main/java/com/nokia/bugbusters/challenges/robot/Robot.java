package com.nokia.bugbusters.challenges.robot;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by pt103441 on 04-04-2015.
 */
enum RadiationLevels {

    NONE(0), CLEAR(10), MINOR(100), MAJOR(500), CRITICAL(501);
    private final int id;

    private RadiationLevels(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }

    public static RadiationLevels getRadiationByLevel(long level) {
        if (level < RadiationLevels.CLEAR.getValue()) {
            return RadiationLevels.CLEAR;
        } else if (level < RadiationLevels.MINOR.getValue()) {
            return RadiationLevels.MINOR;
        } else if (level < RadiationLevels.MAJOR.getValue()) {
            return RadiationLevels.MAJOR;
        } else {
            return RadiationLevels.CRITICAL;
        }
    }

}

enum LogLevel {

    IGNORE("Ignore"), CREATE("Create"), CANCEL("Cancel"),
    ACTIVATE_SHIELD("Activate Shield"),
    DEACTIVATE_SHIELD("Deactivate Shield");
    private final String level;

    private LogLevel(String level) {
        this.level = level;
    }

    public String getValue() {
        return level;
    }
}

class RobotSolver {

    private final InputStream input;
    private final PrintStream output;

    RobotSolver(InputStream input, PrintStream output) throws IOException {
        this.input = input;
        this.output = output;
    }

    public void solve() {
        Scanner scanner = new Scanner(input, "UTF-8");
        while (scanner.hasNext()) {
            handleEvent(scanner.nextLine());
        }
    }

    private void handleEvent(String stringEvent) {
        final Event event = new Event(stringEvent.trim());
        final String robotName = event.getRobotName();
        final Robot robot = RobotFactory.getRobot(robotName);     
        output.println(robot.execute(event));
    }
}

public class Robot {

    private static final String OUTPUT_FORMAT = "%s %s %s";
    private static final String EMPTY_STRING = "";
    private static final String NEW_LINE = System.lineSeparator();
    private final String name;
    private boolean shieldActive;
    private Event lastEvent;

    public Robot() {
        name = "";
    }

    public Event getEvent() {
        return lastEvent;
    }

    public void setEvent(Event event) {
        this.lastEvent = event;
    }

    public Robot(String name) {
        this.name = name;
    }

    public boolean isShield() {
        return shieldActive;
    }

    public void setShield(boolean shield) {
        this.shieldActive = shield;
    }

    public String execute(Event newEvent) {
        StringBuilder builder = new StringBuilder();

        if (newEvent.equals(this.lastEvent)) {
            return ignoreEvent(newEvent);
        } else {
            final boolean isRadiationLevelClearInNewEvent = newEvent.getRadiationLevelLongValue() < RadiationLevels.CLEAR.getValue();
            if (null == lastEvent) {
                if (isRadiationLevelClearInNewEvent) {
                    builder.append(ignoreEvent(newEvent));
                } else {
                    createEvent(builder, newEvent);
                }
            } else {
                builder.append(formatOutput(LogLevel.CANCEL, this.lastEvent.getRawEvent(), this.lastEvent.getRadiationLevel().toString()));
                if (isRadiationLevelClearInNewEvent) {
                    builder.append(threatShield(newEvent));
                    builder.append(NEW_LINE);
                    builder.append(ignoreEvent(newEvent));
                    this.lastEvent = null;
                } else {
                    builder.append(NEW_LINE);
                    createEvent(builder, newEvent);
                }
            }

        }
        return builder.toString();
    }

    private void createEvent(StringBuilder builder, Event newEvent) {
        builder.append(formatOutput(LogLevel.CREATE, newEvent.getRawEvent(), newEvent.getRadiationLevel().toString()));
        builder.append(threatShield(newEvent));
        this.lastEvent = newEvent;
    }

    private String ignoreEvent(Event newEvent) {
        boolean isRadiationLevelClear = newEvent.getRadiationLevel().equals(RadiationLevels.CLEAR);
        String level = isRadiationLevelClear ? RadiationLevels.NONE.name() : newEvent.getRadiationLevel().toString();
        return formatOutput(LogLevel.IGNORE, newEvent.getRawEvent(), level);
    }

    public String formatOutput(LogLevel logLevel, String event, String level) {
        final boolean isShieldMessage = logLevel.equals(LogLevel.ACTIVATE_SHIELD);
        String levelOuput = isShieldMessage ? EMPTY_STRING : level;
        return String.format(OUTPUT_FORMAT, logLevel.getValue(), event, levelOuput).trim();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.name.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Robot other = (Robot) obj;
        return this.name.equals(other.name);
    }

    public static void main(String args[]) throws IOException {
        RobotSolver robotSolver = new RobotSolver(System.in, System.out);
        robotSolver.solve();
    }

    private String threatShield(Event newEvent) {
        boolean levelIsMajorOrCritical = newEvent.getRadiationLevelLongValue() >= RadiationLevels.MINOR.getValue();
        boolean shieldNotActive = !this.shieldActive;
        String toReturn = EMPTY_STRING;
        if (levelIsMajorOrCritical && shieldNotActive) {
            this.setShield(true);
            toReturn = NEW_LINE.concat(formatOutput(LogLevel.ACTIVATE_SHIELD, this.name, EMPTY_STRING));
        } else if (!levelIsMajorOrCritical && !shieldNotActive) {
            this.setShield(false);
            toReturn = NEW_LINE.concat(formatOutput(LogLevel.DEACTIVATE_SHIELD, this.name, EMPTY_STRING));
        }
        return toReturn;

    }

}

class RobotFactory {

    private static final Map<String, Robot> MAP_OF_RULES = new HashMap<>();

    public static Robot getRobot(String robotName) {
        if (MAP_OF_RULES.containsKey(robotName)) {
            return MAP_OF_RULES.get(robotName);
        } else {
            Robot robot = new Robot(robotName);
            MAP_OF_RULES.put(robotName, robot);
            return robot;
        }
    }

    /**
     * martelado para fazer aqui uma cena nos testes unitÃ¡rios
     */
    public static void cleanMap() {
        MAP_OF_RULES.clear();
    }
}
