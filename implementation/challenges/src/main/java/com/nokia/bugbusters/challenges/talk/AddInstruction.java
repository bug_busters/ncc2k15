package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class AddInstruction extends Instruction {
    public AddInstruction(int target, int source) {
        super( target,source);
    }

    //  3rn - Add value n (between 0 and 9) to register r
    public void execute( Computer computer ){
        int inputValue = this.getSource();

        ComputerCpuRegister targetRegister = computer.getRegister( this.getTarget() );
        int registerValue = targetRegister.getValue();
        targetRegister.setValue( registerValue + inputValue );

    }

}
