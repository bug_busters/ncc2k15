package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class AddWithRegisterInstruction extends Instruction {
    public AddWithRegisterInstruction(int target, int source) {
        super( target,source);
    }

    //6rs - Add the value of register s to register r
    public void execute( Computer computer ){
        int sourceRegisterValue = computer.getRegister( this.getSource() ).getValue();
        ComputerCpuRegister targetRegister = computer.getRegister( this.getTarget() );
        int targetRegisterValue = targetRegister.getValue();

        targetRegister.setValue( targetRegisterValue + sourceRegisterValue  );

    }
}
