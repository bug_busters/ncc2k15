package com.nokia.bugbusters.challenges.talk;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pt103441 on 04-04-2015.
 */

public class Computer{
    private final int memSize;
    private final int nrOfRegisters;
    private ComputerRamCell[] memory;
    private ComputerCpuRegister[] cpuRegisters;
    private boolean computerHalted = false;
    private int instructionPointer;
    private List<Integer> program = new ArrayList<>(0);
    private boolean computerError = false;

    public Computer( int memSize, int nrOfRegisters ){
        this.memSize = memSize;
        this.nrOfRegisters = nrOfRegisters;
        boot();
    }

    public final void boot() {
        computerHalted = false;
        computerError = false;
        this.resetInstructionPointer();
        initRegisters();
        initMemory();
        resetInstructionPointer();
    }

    private void resetInstructionPointer() {
        this.instructionPointer = 0;
    }

    private void initMemory() {
        memory = new ComputerRamCell[ memSize ];

        int ramAddress = loadProgramIntoStartOfMemory();

        for( ; ramAddress < memory.length ; ramAddress++  ){
            setValueAtRamAddress(ramAddress, 0);
        }
    }

    private int loadProgramIntoStartOfMemory() {
        int currRamAddress = 0 ;
        for( Integer opcode: this.program ){
            setValueAtRamAddress(currRamAddress, opcode);
            currRamAddress++;
        }
        return currRamAddress;
    }

    public void setValueAtRamAddress(int ramAddress, Integer value) {
        if( ramAddress < memory.length ) {
            memory[ramAddress] = new ComputerRamCell(value);
        } // else { throw new InvalidMemoryAddressAccessException(); }
    }

    private void initRegisters() {
        cpuRegisters = new ComputerCpuRegister[ nrOfRegisters ];
        for (int i = 0; i < cpuRegisters.length; i++) {
            cpuRegisters[i] = new ComputerCpuRegister();
        }
    }

    public void halt() {
        computerHalted = true;
    }

    public ComputerCpuRegister getRegister( int register ) {
        return this.cpuRegisters[ register ];
    }

    public int getValueOfMemoryCell(int memCellAddr){
        return this.memory[ memCellAddr ].getValue();
    }

    public void goTo(int memAddress) {
        this.instructionPointer = memAddress-1;
    }

    public void loadProgram(List<Integer> program) {
        this.program = program;
    }

    public int runProgram(){
        int nrOfExecutedInstructions = 0;
        boot();

        while( ! computerHalted ){
            executeNextInstruction();

            if( ! invalidState() ) {
                nrOfExecutedInstructions++;
            }
        }

        return nrOfExecutedInstructions;
    }

    private void executeNextInstruction() {
        Instruction instructionToExecute = this.memory[ instructionPointer ].getInstruction();
        instructionToExecute.execute(this);
        instructionPointer++;
    }

    public boolean invalidState(){
        return computerError;
    }

    public void error() {
        computerError = true;
    }
}
