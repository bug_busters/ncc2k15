package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 22-04-2015.
 */
public class ComputerRamCell extends ComputerStorageUnit {
    private Instruction instruction;

    public ComputerRamCell( int value ){
        super( value );
        this.instruction = InstructionFactory.create( value );
    }

    public void execute( Computer computer ){
        this.instruction.execute( computer );
    }

    public Instruction getInstruction() {
        return instruction;
    }
}
