package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 22-04-2015.
 */
public class ComputerStorageUnit {
    public static final int MAXVALUE = 1000;
    private int value = 0;

    public ComputerStorageUnit(){}

    public ComputerStorageUnit(int value){
        setValue( value );
    }

    public void setValue(int value) {
        this.value = value % MAXVALUE;
    }

    public int getValue() {
        return value;
    }

}
