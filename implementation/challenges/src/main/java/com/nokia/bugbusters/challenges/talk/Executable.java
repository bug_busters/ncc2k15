package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 21-04-2015.
 */
public interface Executable {
    void execute(Computer computer);
}
