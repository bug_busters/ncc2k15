package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 23-04-2015.
 */
public class GotoIfNotZeroInstruction extends Instruction {
    public GotoIfNotZeroInstruction(int r, int s) {
        super( r, s );
    }

    //0rs - Go to the address in register r unless register s contains 0
    public void execute( Computer computer ){

        ComputerCpuRegister registerWithRamAddress = computer.getRegister( this.getTarget() );
        ComputerCpuRegister registerWithValue = computer.getRegister( this.getSource() );

        int memAddress = registerWithRamAddress.getValue();
        int value = registerWithValue.getValue();

        if( value != 0 ){
            computer.goTo( memAddress );
        }

    }

}
