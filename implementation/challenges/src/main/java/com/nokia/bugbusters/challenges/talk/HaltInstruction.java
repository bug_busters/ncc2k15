package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 17-04-2015.
 */
public class HaltInstruction extends Instruction {
    public HaltInstruction(){
        super(0,0);
    }

    @Override
    public void execute(Computer computer ){
        computer.halt();
    }
}
