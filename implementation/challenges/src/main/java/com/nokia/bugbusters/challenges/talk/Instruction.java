package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 15-04-2015.
 */

/*
Instruction - Meaning
100 - Halt the execution
2rn - Set register r equal to o value n (between 0 and 9)
3rn - Add value n (between 0 and 9) to register r
4rn - Multiply value n (between 0 and 9) to register r
5rs - Set register r to the value of register s
6rs - Add the value of register s to register r
7rs - Multiply register r by the value of register s
8rs - Set register r to the value in RAM whose address is in register s
9rs - Set the value in RAM whose address is in register s to the value of register r
0rs - Go to the address in register r unless register s contains 0
 */

public class Instruction implements Executable{
    public static final int OPCODE_SET = 2;
    public static final int OPCODE_ADD = 3;
    public static final int OPCODE_MULTIPLY = 4;
    public static final int OPCODE_SET_WITH_REGISTER = 5;
    public static final int OPCODE_ADD_WITH_REGISTER = 6;
    public static final int OPCODE_MULTIPLY_WITH_REGISTER = 7;
    public static final int OPCODE_SET_WITH_MEMADDRESS = 8;
    public static final int OPCODE_SET_MEMADDRESS = 9;
    public static final int OPCODE_HALT = 1;
    public static final int OPCODE_GOTO = 0;

    private int source;
    private int target;


    public Instruction(int target, int source) {
        this.source = source;
        this.target = target;
    }


    public void execute(Computer computer) {
        return;
    }

    public int getSource() {
        return source;
    }

    public int getTarget() {
        return target;
    }

    public String toString(){
        return "Executing instruction: " + this.getClass().getSimpleName() + " source: " + getSource() + " target: " + getTarget();
    }


}
