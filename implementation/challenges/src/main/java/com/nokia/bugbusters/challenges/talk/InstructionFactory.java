package com.nokia.bugbusters.challenges.talk;

        

/**
 * Created by pt103441 on 15-04-2015.
 */
public class InstructionFactory {


    /*
    Instruction - Meaning
    100 - Halt the execution
    2rn - Set register r equal to value n (between 0 and 9)
    3rn - Add value n (between 0 and 9) to register r
    4rn - Multiply value n (between 0 and 9) to register r
    5rs - Set register r to the value of register s
    6rs - Add the value of register s to register r
    7rs - Multiply register r by the value of register s
    8rs - Set register r to the value in RAM whose address is in register s
    9rs - Set the value in RAM whose address is in register s to the value of register r
    0rs - Go to the address in register r unless register s contains 0
     */


    public static Instruction create( int instructionCode ){

        int firstDigit = instructionCode / 100;
        int secondDigit = instructionCode % 100 / 10;
        int thirdDigit = instructionCode % 100 % 10;

        int opcode = firstDigit;
        int targetRegister = secondDigit;
        int value = thirdDigit;
        int registerWithValue = value;
        int r = secondDigit;
        int s = thirdDigit;

        switch( opcode ){
            case Instruction.OPCODE_HALT:
                return new HaltInstruction();
            case Instruction.OPCODE_SET:
                return new SetInstruction( targetRegister, value );
            case Instruction.OPCODE_ADD:
                return new AddInstruction( targetRegister, value );
            case Instruction.OPCODE_MULTIPLY:
                return new MultiplyInstruction( targetRegister, value );
            case Instruction.OPCODE_SET_WITH_REGISTER:
                return new SetWithRegisterInstruction( targetRegister, registerWithValue );
            case Instruction.OPCODE_ADD_WITH_REGISTER:
                return new AddWithRegisterInstruction( targetRegister, registerWithValue );
            case Instruction.OPCODE_MULTIPLY_WITH_REGISTER:
                return new MultiplyWithRegisterInstruction( targetRegister, registerWithValue );
            case Instruction.OPCODE_SET_WITH_MEMADDRESS:
                return new SetWithMemAddressInstruction( s , r );
            case Instruction.OPCODE_SET_MEMADDRESS:
                return new SetMemAddressInstruction( s , r );
            case Instruction.OPCODE_GOTO:
                return new GotoIfNotZeroInstruction( r, s );
            default:
                return new UnknownInstruction( targetRegister, value );
        }

    }
}
