package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class MultiplyInstruction extends Instruction {
    public MultiplyInstruction(int target, int source) {
        super( target,source);
    }

    //4rn - Multiply value n (between 0 and 9) to register r
    public void execute( Computer computer ){
        int inputValue = this.getSource();

        ComputerCpuRegister targetRegister = computer.getRegister( this.getTarget() );
        int registerValue = targetRegister.getValue();
        targetRegister.setValue( registerValue * inputValue );

    }

}
