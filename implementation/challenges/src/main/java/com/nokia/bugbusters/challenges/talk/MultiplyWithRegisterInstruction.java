package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class MultiplyWithRegisterInstruction extends Instruction {
    public MultiplyWithRegisterInstruction(int target, int source) {
        super( target,source);
    }

    //7rs - Multiply register r by the value of register s
    public void execute( Computer computer ){
        int sourceRegisterValue = computer.getRegister( this.getSource() ).getValue();
        ComputerCpuRegister targetRegister = computer.getRegister( this.getTarget() );
        int targetRegisterValue = targetRegister.getValue();

        targetRegister.setValue( targetRegisterValue * sourceRegisterValue  );

    }
}
