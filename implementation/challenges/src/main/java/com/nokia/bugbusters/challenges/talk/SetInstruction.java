package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class SetInstruction extends Instruction {
    public SetInstruction(int targetRegister, int value) {
        super( targetRegister, value );
    }

    //3rn - Add value n (between 0 and 9) to register r
    public void execute( Computer computer ){
        int inputValue = this.getSource();

        ComputerCpuRegister targetRegister = computer.getRegister( this.getTarget() );
        targetRegister.setValue( inputValue );

    }

}
