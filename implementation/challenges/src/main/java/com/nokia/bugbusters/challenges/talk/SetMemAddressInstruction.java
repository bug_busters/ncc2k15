package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class SetMemAddressInstruction extends Instruction {
    public SetMemAddressInstruction(int target, int source) {
        super(target, source);
    }

    //9rs - Set the value in RAM whose address is in register s to the value of register r
    public void execute( Computer computer ){

        ComputerCpuRegister registerWithRamAddress = computer.getRegister( this.getTarget() );
        ComputerCpuRegister registerWithValueToSet = computer.getRegister( this.getSource() );

        int memAddress = registerWithRamAddress.getValue();
        int valueToSet = registerWithValueToSet.getValue();

        computer.setValueAtRamAddress(memAddress , valueToSet );

    }

}
