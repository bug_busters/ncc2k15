package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class SetWithMemAddressInstruction extends Instruction {
    public SetWithMemAddressInstruction(int target, int source) {
        super( source, target);
    }

    //8rs - Set register r to the value in RAM whose address is in register s
    public void execute( Computer computer ){

        ComputerCpuRegister sourceRegister = computer.getRegister(this.getSource());


        int sourceValueFromRam = computer.getValueOfMemoryCell( sourceRegister.getValue() );


        ComputerCpuRegister targetRegister = computer.getRegister( this.getTarget() );
        targetRegister.setValue( sourceValueFromRam  );

    }

}
