package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class SetWithRegisterInstruction extends Instruction {
    public SetWithRegisterInstruction(int target, int source) {
        super( target,source);
    }

    //5rs - Set register r to the value of register s
    public void execute( Computer computer ){
        ComputerCpuRegister sourceRegister = computer.getRegister( this.getSource() );
        ComputerCpuRegister targetRegister = computer.getRegister( this.getTarget() );
        targetRegister.setValue( sourceRegister.getValue()  );

    }
}
