package com.nokia.bugbusters.challenges.talk;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


class TalkSolver {

    public static final int MEMSIZE = 1000;
    public static  final int NROFREGISTERS = 10;

    private final PrintStream output;
    private String currLine;
    private Scanner lineReader;

    TalkSolver(InputStream input, PrintStream output) throws IOException {
        this.output = output;
        lineReader = new Scanner( input, Charset.defaultCharset().toString() );
    }

    public void solve() {

        Computer computer = new Computer( MEMSIZE, NROFREGISTERS );

        int nrOfPrograms = Integer.parseInt( getNextLineFromInput() );
        getNextLineFromInput();


        getNextLineFromInput();

        for( int progIdx = 0 ; progIdx < nrOfPrograms ; progIdx++ ){
            if( progIdx != 0 ){
                output.println();
            }

            List<Integer> program = readProgram();
            computer.loadProgram(program);
            int nrInstructionsExecuted = computer.runProgram();
            output.println( nrInstructionsExecuted );
            getNextLineFromInput();
        }

    }

    private List<Integer> readProgram() {
        List<Integer> loadingProgram = new ArrayList<>();
        while( currLine != null && ! currLine.equals("") ) {
            loadingProgram.add( Integer.parseInt(currLine) );
            getNextLineFromInput();
        }
        return  loadingProgram;
    }

    private String getNextLineFromInput() {
        if( lineReader.hasNext() ) {
            currLine = lineReader.nextLine();
        } else {
            currLine = null;
        }
        return currLine;

    }


}

public class Talk {

    public static void main(String args[]) throws IOException {
        TalkSolver talkSolver = new TalkSolver(System.in, System.out);
        talkSolver.solve();
    }

}
