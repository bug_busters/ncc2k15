package com.nokia.bugbusters.challenges.talk;

/**
 * Created by pt103441 on 16-04-2015.
 */
public class UnknownInstruction extends Instruction {
    public UnknownInstruction(int target, int source) {
        super(target, source);
    }

    public void execute(Computer computer) {
        computer.halt();
        computer.error();
    }
}
