package com.nokia.bugbusters.challenges.amazing;

import com.nokia.bugbusters.util.AcceptanceTestSpec;
import com.nokia.bugbusters.util.DynamicEntityTest;
import org.junit.runners.Parameterized.Parameters;


public class AmazingEntityTest extends DynamicEntityTest {

    @Parameters(name="{0}")
    public static Iterable<Object[]> generateTestsParameters() {
        
    	return generateTestsParametersForCodeChallengeProblem( "amazing" ); //You can use other methods. See below note
        
    }


    public AmazingEntityTest(String testName, AcceptanceTestSpec testSpec) {
        super( testName, testSpec );
    }

}
