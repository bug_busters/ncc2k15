package com.nokia.bugbusters.challenges.amazing;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class AmazingUnitTest {

	private OutputStream output = null;
	private OutputStream errors = null;
	
	@Before
	public void setup(){
		output = new ByteArrayOutputStream();
		errors = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));
		System.setErr(new PrintStream(errors));
	}
	
	@Test
	public void build_1_Maze_From_Input_With_Size_1x1_Test() throws NumberFormatException, IOException{
		String input =  "1"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"1x1" + System.lineSeparator() +  
						"1"   + System.lineSeparator();
	
		InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		AmazingSolver solver = new AmazingSolver(inputStream,output);
		Assert.assertEquals(1, solver.getMazes().size());
		Assert.assertEquals(1, solver.getMaze(0).size());
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),1,0,0);
	}
	
	@Test
	public void build_1_Maze_From_Input_With_Size_2x1_Test() throws NumberFormatException, IOException{
		String input =  "1"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x1" + System.lineSeparator() +  
						"1"   + System.lineSeparator() +
						"2"   + System.lineSeparator();
	
		InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		AmazingSolver solver = new AmazingSolver(inputStream,output);
		Assert.assertEquals(1, solver.getMazes().size());
		Assert.assertEquals(2, solver.getMaze(0).size());
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),1,0,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),2,1,0);
	}
	
	@Test
	public void build_1_Maze_From_Input_With_Size_2x2_Test() throws NumberFormatException, IOException{
		String input =  "1"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x2" + System.lineSeparator() +  
						"1 3"   + System.lineSeparator() +
						"2 4"   + System.lineSeparator();
	
		InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		AmazingSolver solver = new AmazingSolver(inputStream,output);
		Assert.assertEquals(1, solver.getMazes().size());
		Assert.assertEquals(4, solver.getMaze(0).size());
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),1,0,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),2,1,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),3,0,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),4,1,1);
	}
	
	

	@Test
	public void build_1_Maze_From_Input_With_Size_3x4_Test() throws NumberFormatException, IOException{
		String input =  "1"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"3x4" + System.lineSeparator() +  
						"1 3 5 6"   + System.lineSeparator() +
						"2 4 7 8"   + System.lineSeparator() +
						"9 10 11 12"   + System.lineSeparator();
	
		InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		AmazingSolver solver = new AmazingSolver(inputStream,output);
		Assert.assertEquals(1, solver.getMazes().size());
		Assert.assertEquals(12, solver.getMaze(0).size());
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),1,0,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),2,1,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),3,0,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),4,1,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),5,0,2);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),6,0,3);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),7,1,2);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),8,1,3);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),9,2,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),10,2,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),11,2,2);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),12,2,3);
	}
	
	@Test
	public void build_2_Mazes_From_Input_With_Diferent_Sizes_Test() throws NumberFormatException, IOException{
		String input =  "2"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x2" + System.lineSeparator() +  
						"1 3"   + System.lineSeparator() +
						"2 4"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x3" + System.lineSeparator() +  
						"1 3 5"   + System.lineSeparator() +
						"2 4 6"   + System.lineSeparator();
	
		InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		AmazingSolver solver = new AmazingSolver(inputStream,output);
		Assert.assertEquals(2, solver.getMazes().size());
		Assert.assertEquals(4, solver.getMaze(0).size());
		Assert.assertEquals(6, solver.getMaze(1).size());
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),1,0,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),2,1,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),3,0,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(0),4,1,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(1),1,0,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(1),2,1,0);
		check_that_coordinates_for_value_is_equal(solver.getMaze(1),3,0,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(1),4,1,1);
		check_that_coordinates_for_value_is_equal(solver.getMaze(1),5,0,2);
		check_that_coordinates_for_value_is_equal(solver.getMaze(1),6,1,2);
	}
	
	@Test
	public void solve_2_mazes_Test() throws NumberFormatException, IOException{
		String input =  "2"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x2" + System.lineSeparator() +  
						"1 3"   + System.lineSeparator() +
						"2 4"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x3" + System.lineSeparator() +  
						"1 3 6"   + System.lineSeparator() +
						"2 4 5"   + System.lineSeparator();
	
		InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Amazing.main(new String[0]);
		Assert.assertEquals("2 1" + System.lineSeparator() + "4 3"+ System.lineSeparator(), output.toString());
	}
	
	@Test
	public void solve_3_mazes_Test() throws NumberFormatException, IOException{
		String input =  "3"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x2" + System.lineSeparator() +  
						"3 4"   + System.lineSeparator() +
						"1 2"   + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"3x3" + System.lineSeparator() +  
						"9 2 3"   + System.lineSeparator() +
						"4 5 6"   + System.lineSeparator() + 
						"7 8 1" + System.lineSeparator() +
						""    + System.lineSeparator() + 
						"2x4" + System.lineSeparator() +  
						"8 2 3 4"   + System.lineSeparator() +
						"1 6 7 5"   + System.lineSeparator();
		InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Amazing.main(new String[0]);
		Assert.assertEquals("2 1" + System.lineSeparator() + "3 4" + System.lineSeparator() + "4 2"+ System.lineSeparator(),  output.toString());
	}
	
	
	private void check_that_coordinates_for_value_is_equal(Maze maze,int value, int row, int columm) {
		Assert.assertEquals(row,maze.getSquares()[value].getRow());
		Assert.assertEquals(columm,maze.getSquares()[value].getColumm());
	}
	
}
