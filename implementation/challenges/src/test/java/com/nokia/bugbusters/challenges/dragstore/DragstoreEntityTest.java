package com.nokia.bugbusters.challenges.dragstore;

import com.nokia.bugbusters.util.AcceptanceTestSpec;
import com.nokia.bugbusters.util.DynamicEntityTest;
import org.junit.runners.Parameterized.Parameters;

 /*

USAGE:

    This dynamic entity test generates a _JUnit test_ per input file (.in extension) in the
    specified tests root directory and in its subdirectories (it searches recursively).

    The difference from the EntityTest class is that:
        - _each_ test is run as it was launched from the command line
        - One JUnit test is run for _each_ input file


    How:

    1st - Create a new class that extends DynamicEntityTest

            (you can just copy this one and change the class name and the problem name string below)

    2nd - Add a constructor exactly as below.

             (receives a testSpec as parameter and just calls the super constructor with that parameter)

    3rd - Create a static method that returns the tests parameters as  Iterable<Object[]> annotated with "@Parameters"

            (like below generateTestsParameters method)


*/


public class DragstoreEntityTest extends DynamicEntityTest {

    @Parameters(name="{0}")
    public static Iterable<Object[]> generateTestsParameters() {
        return generateTestsParametersForCodeChallengeProblem( "dragstore" , "com.nokia.bugbusters.challenges.dragstore.Main"); //You can use other methods. See below note

/*
            NOTE:

            To generate the tests parameters (your main class name, the tests root dir and where the java classes are)
            you can use one of 3 methods according to the assumptions that can be made:


            Helper method 1 - just the problem name in small caps:

                generateTestsParametersForCodeChallengeProblem(String codeChallengeProblemName )
                --------------------------------------------------------------------------------

                e.g.: generateTestsParametersForCodeChallengeProblem( "iteratio" )

                NOTE: It assumes the class with the main method is called:
                    "com.nokia.bugbusters.challenges." + PROBLEM_NAME + "." + capitalizeFirstLetter(PROBLEM_NAME);
                    e.g.: "com.nokia.bugbusters.challenges.iteratio.Iteratio"


            Helper method 2 - the full Class name that has the main method and with the problem name in small caps :

                generateTestsParametersForCodeChallengeProblem(String codeChallengeProblemName, String mainClassName)
                -----------------------------------------------------------------------------------------------------

                e.g.: return generateTestsParametersForCodeChallengeProblem( "iteratio" , "com.nokia.bugbusters.challenges.iteratio.Main" );


            Helper method 3 - with everything specified (using the AcceptanceTestsRuntime class):

                getTestFilesAsTestsParameters(AcceptanceTestsRuntime acceptanceTestsRuntime)
                ----------------------------------------------------------------------------

                e.g.:

                   String currPath = ClassLoader.getSystemClassLoader().getResource("").getPath().toString();

                   AcceptanceTestsRuntime testsRuntime = new AcceptanceTestsRuntime(
                            "com.nokia.bugbusters.challenges.iteratio.Main",
                            currPath + "..\\classes",
                            currPath + "..\\..\\resources\\iteratio\\tests"
                    );

                    return getTestFilesAsTestsParameters( testsRuntime );

 */

    }


    public DragstoreEntityTest(String testName, AcceptanceTestSpec testSpec) {
        super( testName, testSpec );
    }

}
