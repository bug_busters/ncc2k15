package com.nokia.bugbusters.challenges.dragstore;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
public class MainTest {

    private static final String GLOB_IN = "*.in";
    private static final String GLOB_OUT = "*.out";
    private static final String TEST_PATH = Paths.get(".").toAbsolutePath().normalize().toString().concat("/resources/dragstor/tests");
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private PrintStream printStream = new PrintStream(outContent);
    private PrintStream savedStdoutStream;

    public MainTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        savedStdoutStream = System.out;
        System.setOut(printStream);
    }

    @After
    public void tearDown() {
        System.setOut(savedStdoutStream);
    }

    @Test
    public void testcreateInstruction() {
        String input = "put 1 on 1";
        Main instance = new Main(null, printStream);
        InstructionParameters expectedResult = new InstructionParameters("put", 1l, 1l);

        assertEquals(expectedResult, instance.createInstruction(input));
        input = "put 1 on 2";
        assertNotSame(instance, instance.createInstruction(input));

        expectedResult = new InstructionParameters("get", 1l, Long.MAX_VALUE);
        input = "get 1";
        System.out.println(instance.createInstruction(input));
        assertEquals(expectedResult, instance.createInstruction(input));
    }
}
