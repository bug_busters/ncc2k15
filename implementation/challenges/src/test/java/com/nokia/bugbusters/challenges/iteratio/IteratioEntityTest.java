package com.nokia.bugbusters.challenges.iteratio;

import com.nokia.bugbusters.util.AcceptanceTestSpec;
import com.nokia.bugbusters.util.DynamicEntityTest;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

public class IteratioEntityTest extends DynamicEntityTest {

    @Test(timeout=1000)
    public void test() {
        super.testExecution();
    }

    @Parameters(name="{0}")
    public static Iterable<Object[]> generateTestsParameters() {
        return generateTestsParametersForCodeChallengeProblem( "iteratio" ); 
    }


    public IteratioEntityTest(String testName, AcceptanceTestSpec testSpec) {
        super( testName, testSpec );
    }

}
