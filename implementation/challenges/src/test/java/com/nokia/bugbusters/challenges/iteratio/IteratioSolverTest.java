package com.nokia.bugbusters.challenges.iteratio;

import com.nokia.bugbusters.util.Chronometer;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IteratioSolverTest {

    @Test
    public void testCase01FIRST() {
        Chronometer cron = new Chronometer("testCase_FIRST");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 1000000);
        cron.stopCounting();
        assertTrue("too Slow", cron.getMilliseconds() <= 1000L);
        assertEquals("525 837799", interval.toString());
        System.out.println( cron.toString());
    }
    @Test
    public void testCase1() {

        Chronometer cron = new Chronometer("testCase1");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1,10);
        assertTrue( cron.getMilliseconds() <= 1000L );
        assertEquals("20 9", interval.toString());
    }

    @Test
    public void testCase2() {
        Chronometer cron = new Chronometer("testCase2");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 100, 200);
        assertTrue( cron.getMilliseconds() <= 1000L );
        assertEquals("125 171", interval.toString());
    }

    @Test
    public void testCase3() {
        Chronometer cron = new Chronometer("testCase3");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 201, 210);
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals( "89 206 207", interval.toString());
    }

    @Test
    public void testCase4() {
        Chronometer cron = new Chronometer("testCase4");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 900, 1000);
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals("174 937", interval.toString());
    }

    @Test
    public void testCase1000() {
        Chronometer cron = new Chronometer("testCase_1.000");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 1000);
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals("179 871", interval.toString());
    }

    @Test
    public void testCase10000() {
        Chronometer cron = new Chronometer("testCase_10.000");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 10000);
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals("262 6171", interval.toString());
    }

    @Test
    public void testCase50000() {
        Chronometer cron = new Chronometer("testCase_50.000");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 50000);
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals("324 35655", interval.toString());
    }

    @Test
    public void testCase100000() {
        Chronometer cron = new Chronometer("testCase_100.000");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 100000);
        cron.stopCounting();
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals("351 77031", interval.toString());
        System.out.println( cron.toString());
    }
    @Test
    public void testCase200000() {
        Chronometer cron = new Chronometer("testCase_200.000");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 200000);
        cron.stopCounting();
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals("383 156159", interval.toString());
        System.out.println( cron.toString());
    }
    @Test
    public void testCase500000() {
        Chronometer cron = new Chronometer("testCase_500.000");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 500000);
        cron.stopCounting();
        assertTrue(cron.getMilliseconds() <= 1000L);
        assertEquals("449 410011", interval.toString());
        System.out.println( cron.toString());
    }
    @Test
    public void testCase1000000() {
        Chronometer cron = new Chronometer("testCase_1.000.000");
        MaximumIterationsForInterval interval = new MaximumIterationsForInterval( 1, 1000000);
        cron.stopCounting();
        assertTrue("too Slow", cron.getMilliseconds() <= 1000L);
        assertEquals("525 837799", interval.toString());
        System.out.println( cron.toString());
    }
    @Test
    public void testCase_Stream() {
        String nl = System.lineSeparator();
        String standardInput = "2"+nl+"1 10"+nl+"1 20";
        String standardOutput = "20 9"+nl+"21 18 19"+nl;

        Chronometer cron = new Chronometer("testCase_stream");
        IteratioSolver solver = null;
        String solution= "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(2024);
            solver = new IteratioSolver(IOUtils.toInputStream(standardInput), new PrintStream(baos));
            solver.solve();
            solution = baos.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        cron.stopCounting();
        assertTrue("too Slow", cron.getMilliseconds() <= 1000L);
        assertEquals( standardOutput, solution.toString());
        System.out.println(cron.toString());
    }

}