package com.nokia.bugbusters.challenges.knock;

import com.nokia.bugbusters.util.AcceptanceTestSpec;
import com.nokia.bugbusters.util.DynamicEntityTest;
import org.junit.runners.Parameterized.Parameters;


public class KnockEntityTest extends DynamicEntityTest {

    @Parameters(name="{0}")
    public static Iterable<Object[]> generateTestsParameters() {
        return generateTestsParametersForCodeChallengeProblem( "knock" ); 
    }


    public KnockEntityTest(String testName, AcceptanceTestSpec testSpec) {
        super( testName, testSpec );
    }

}
