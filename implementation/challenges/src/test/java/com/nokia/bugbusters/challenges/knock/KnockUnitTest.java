package com.nokia.bugbusters.challenges.knock;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Test;

public class KnockUnitTest {
	
	@Test
	public void create_a_knock_solver() throws IOException{
		String inputString = "Some String" + System.lineSeparator();
		OutputStream outputStream = System.out;
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		KnockSolver solver = new KnockSolver(inputStream , outputStream);
		Assert.assertNotNull(solver);
	}
	
	@Test
	public void test_reading_input_with_1_problem_with_1_room() throws IOException{
		String inputString = "1" + System.lineSeparator() +
							"1 1" + System.lineSeparator() + 
							"x x x x";
		OutputStream outputStream = System.out;
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		KnockSolver solver = new KnockSolver(inputStream , outputStream);
		Assert.assertNotNull(solver);
		Assert.assertEquals(1, solver.getProblem(0).getRooms().size());
	}
	
	@Test
	public void test_solving_1_problem_with_1_room() throws IOException{
		String inputString = "1" + System.lineSeparator() +
							"1 1" + System.lineSeparator() + 
							"x x x x";
		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("1 1"+ System.lineSeparator(), outputStream.toString());
	}
	
	@Test
	public void test_solving_1_problem_with_2_room() throws IOException{
		String inputString = "1" + System.lineSeparator() +
							"2 1" + System.lineSeparator() + 
							"x x x 2" + System.lineSeparator() +
							"1 x x x";
		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("2 1"+ System.lineSeparator(), outputStream.toString());
	}
	
	@Test
	public void test_solving_1_problem_with_3_room() throws IOException{
		String inputString = "1" + System.lineSeparator() +
							"3 1" + System.lineSeparator() + 
							"x x x 2" + System.lineSeparator() +
							"1 x x 3" + System.lineSeparator() +
							"2 x x x";
		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("3 1"+ System.lineSeparator(), outputStream.toString());
	}
	
	@Test
	public void test_solving_1_problem_with_4_room() throws IOException{
		String inputString = "1" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x";
		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("4 1"+ System.lineSeparator(), outputStream.toString());
	}
	
	@Test
	public void test_solving_1_problem_with_9_room() throws IOException{
		String inputString = "1" + System.lineSeparator() +
							"3 3" + System.lineSeparator() + 
							"x x 2 5" + System.lineSeparator() +
							"x 1 9 3" + System.lineSeparator() +
							"x 2 x 8" + System.lineSeparator() +
							"1 x 3 4" + System.lineSeparator() +
							"2 5 8 6" + System.lineSeparator() +
							"9 3 x 7" + System.lineSeparator() +
							"5 x 6 x" + System.lineSeparator() +
							"3 4 7 x" + System.lineSeparator() +
							"8 6 x x";

		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("4 6"+ System.lineSeparator(), outputStream.toString());
	}
	
	
	@Test
	public void test_solving_2_problems() throws IOException{
		String inputString = "2" + System.lineSeparator() +
							"3 3" + System.lineSeparator() + 
							"x x 2 5" + System.lineSeparator() +
							"x 1 9 3" + System.lineSeparator() +
							"x 2 x 8" + System.lineSeparator() +
							"1 x 3 4" + System.lineSeparator() +
							"2 5 8 6" + System.lineSeparator() +
							"9 3 x 7" + System.lineSeparator() +
							"5 x 6 x" + System.lineSeparator() +
							"3 4 7 x" + System.lineSeparator() +
							"8 6 x x" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x";

		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("4 6" + System.lineSeparator() + "4 1"+ System.lineSeparator(), outputStream.toString());
	}
	
	@Test
	public void test_solving_3_problems() throws IOException{
		String inputString = "3" + System.lineSeparator() +
							"3 3" + System.lineSeparator() + 
							"x x 2 5" + System.lineSeparator() +
							"x 1 9 3" + System.lineSeparator() +
							"x 2 x 8" + System.lineSeparator() +
							"1 x 3 4" + System.lineSeparator() +
							"2 5 8 6" + System.lineSeparator() +
							"9 3 x 7" + System.lineSeparator() +
							"5 x 6 x" + System.lineSeparator() +
							"3 4 7 x" + System.lineSeparator() +
							"8 6 x x" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x";

		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("4 6" + System.lineSeparator() + "4 1"+ System.lineSeparator() + "4 1"+ System.lineSeparator(), outputStream.toString());
	}
	
	@Test
	public void test_solving_4_problems() throws IOException{
		String inputString = "4" + System.lineSeparator() +
							"3 3" + System.lineSeparator() + 
							"x x 2 5" + System.lineSeparator() +
							"x 1 9 3" + System.lineSeparator() +
							"x 2 x 8" + System.lineSeparator() +
							"1 x 3 4" + System.lineSeparator() +
							"2 5 8 6" + System.lineSeparator() +
							"9 3 x 7" + System.lineSeparator() +
							"5 x 6 x" + System.lineSeparator() +
							"3 4 7 x" + System.lineSeparator() +
							"8 6 x x" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x" + System.lineSeparator() +
							"1 1" + System.lineSeparator() + 
							"x x x x";

		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("4 6" + System.lineSeparator() + "4 1"+ System.lineSeparator() + "4 1"+ System.lineSeparator() + "1 1"+ System.lineSeparator(), outputStream.toString());
	}
	
	@Test
	public void test_solving_5_problems() throws IOException{
		String inputString = "5" + System.lineSeparator() +
							"3 3" + System.lineSeparator() + 
							"x x 2 5" + System.lineSeparator() +
							"x 1 9 3" + System.lineSeparator() +
							"x 2 x 8" + System.lineSeparator() +
							"1 x 3 4" + System.lineSeparator() +
							"2 5 8 6" + System.lineSeparator() +
							"9 3 x 7" + System.lineSeparator() +
							"5 x 6 x" + System.lineSeparator() +
							"3 4 7 x" + System.lineSeparator() +
							"8 6 x x" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x" + System.lineSeparator() +
							"2 2" + System.lineSeparator() + 
							"x x 2 4" + System.lineSeparator() +
							"x 1 x 3" + System.lineSeparator() +
							"1 x 3 x" + System.lineSeparator() +
							"2 4 x x" + System.lineSeparator() +
							"1 1" + System.lineSeparator() + 
							"x x x x" + System.lineSeparator() +
							"5 5" + System.lineSeparator() +
							"x x 4 6" + System.lineSeparator() +
							"x 5 3 7" + System.lineSeparator() +
							"x 4 2 8" + System.lineSeparator() +
							"x 3 1 9" + System.lineSeparator() +
							"x 2 x 10" + System.lineSeparator() +
							"5 x 7 15" + System.lineSeparator() +
							"4 6 8 14" + System.lineSeparator() +
							"3 7 9 13" + System.lineSeparator() +
							"2 8 10 12" + System.lineSeparator() +
							"1 9 x 11" + System.lineSeparator() +
							"6 x 14 16" + System.lineSeparator() +
							"7 15 13 17" + System.lineSeparator() +
							"8 14 12 18" + System.lineSeparator() +
							"9 13 11 19" + System.lineSeparator() +
							"10 12 x 20" + System.lineSeparator() +
							"15 x 17 25" + System.lineSeparator() +
							"14 16 18 24" + System.lineSeparator() +
							"13 17 19 23" + System.lineSeparator() +
							"12 18 20 22" + System.lineSeparator() +
							"11 19 x 21" + System.lineSeparator() +
							"16 x 24 x" + System.lineSeparator() +
							"17 25 23 x" + System.lineSeparator() +
							"18 24 22 x" + System.lineSeparator() +
							"19 23 21 x" + System.lineSeparator() +
							"20 22 x x";

		OutputStream outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
		InputStream inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
		System.setIn(inputStream);
		Knock.main(new String[0]);
		Assert.assertEquals("4 6" + System.lineSeparator() + "4 1"+ System.lineSeparator() + "4 1"+ System.lineSeparator() + "1 1"+ System.lineSeparator() + "25 1"+ System.lineSeparator(), outputStream.toString());
		
	}
		
}
