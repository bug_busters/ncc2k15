package com.nokia.bugbusters.challenges.robot;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rui Batalha <rui.batalha@nokia.com>
 */
public class RobotSolverTest {

    public RobotSolverTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of solve method, of class RobotSolver.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testSolve() throws IOException {

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outContent);

        String input = "1 R22-A 5 29 4"+System.lineSeparator()
                + "2 R22-B 250 40 10"+System.lineSeparator()
                + "3 R22-B 250 40 10"+System.lineSeparator()
                + "4 R22-B 250 40 10"+System.lineSeparator()
                + "5 R22-A 10 30 4"+System.lineSeparator()
                + "6 R22-A 10 30 5"+System.lineSeparator()
                + "7 R22-B 250 40 10"+System.lineSeparator()
                + "8 R22-B 250 40 10"+System.lineSeparator()
                + "9 R22-B 250 40 10"+System.lineSeparator()
                + "10 R22-B 250 41 10"+System.lineSeparator()
                + "11 R22-A 100 31 5"+System.lineSeparator()
                + "12 R22-A 500 32 6"+System.lineSeparator()
                + "13 R22-B 10 40 11"+System.lineSeparator()
                + "14 R22-A 100 33 6"+System.lineSeparator()
                + "15 R22-A 2 34 6";

        ByteArrayInputStream inStream = new ByteArrayInputStream(input.getBytes());
        RobotSolver instance = new RobotSolver(inStream, printStream);

        instance.solve();

        String expectedResult = "Ignore 1 R22-A 5 29 4 NONE"+System.lineSeparator()
                + "Create 2 R22-B 250 40 10 MAJOR"+System.lineSeparator()
                + "Activate Shield R22-B"+System.lineSeparator()
                + "Ignore 3 R22-B 250 40 10 MAJOR"+System.lineSeparator()
                + "Ignore 4 R22-B 250 40 10 MAJOR"+System.lineSeparator()
                + "Create 5 R22-A 10 30 4 MINOR"+System.lineSeparator()
                + "Cancel 5 R22-A 10 30 4 MINOR"+System.lineSeparator()
                + "Create 6 R22-A 10 30 5 MINOR"+System.lineSeparator()
                + "Ignore 7 R22-B 250 40 10 MAJOR"+System.lineSeparator()
                + "Ignore 8 R22-B 250 40 10 MAJOR"+System.lineSeparator()
                + "Ignore 9 R22-B 250 40 10 MAJOR"+System.lineSeparator()
                + "Cancel 2 R22-B 250 40 10 MAJOR"+System.lineSeparator()
                + "Create 10 R22-B 250 41 10 MAJOR"+System.lineSeparator()
                + "Cancel 6 R22-A 10 30 5 MINOR"+System.lineSeparator()
                + "Create 11 R22-A 100 31 5 MAJOR"+System.lineSeparator()
                + "Activate Shield R22-A"+System.lineSeparator()
                + "Cancel 11 R22-A 100 31 5 MAJOR"+System.lineSeparator()
                + "Create 12 R22-A 500 32 6 CRITICAL"+System.lineSeparator()
                + "Cancel 10 R22-B 250 41 10 MAJOR"+System.lineSeparator()
                + "Create 13 R22-B 10 40 11 MINOR"+System.lineSeparator()
                + "Deactivate Shield R22-B"+System.lineSeparator()
                + "Cancel 12 R22-A 500 32 6 CRITICAL"+System.lineSeparator()
                + "Create 14 R22-A 100 33 6 MAJOR"+System.lineSeparator()
                + "Cancel 14 R22-A 100 33 6 MAJOR"+System.lineSeparator()
                + "Deactivate Shield R22-A"+System.lineSeparator()
                + "Ignore 15 R22-A 2 34 6 NONE"+System.lineSeparator();

        
        assertEquals(expectedResult, outContent.toString());

//        input = "1 R22-B 250 40 10\n2 R22-B 250 40 10\n3 R22-A 5 29 4\n4 R22-B 9 30 5\n";
//
//        expectedResult = "Create 1 R22-B 250 40 10 MAJOR\nActivate Shield R22-B\nIgnore 2 R22-B 250 40 10 MAJOR\nIgnore 3 R22-A 5 29 4 NONE\n"
//                + "Cancel 1 R22-B 250 40 10 MAJOR\nCreate 4 R22-B 9 30 5 CLEAR\nDeactivate Shield R22-B\n";
////        
//        outContent.reset();
//        outContent.close();
//        printStream.close();
//        outContent = new ByteArrayOutputStream();
//        printStream = new PrintStream(outContent);
//        inStream = new ByteArrayInputStream(input.getBytes());
//
//        RobotFactory.cleanMap();
//
//        instance = new RobotSolver(inStream, printStream);
//
//        instance.solve();
//
//        assertEquals(expectedResult, outContent.toString());

    }

    /**
     * Test of formatOutput method, of class RobotSolver.
     */
    @Test
    public void testPrintOutput() {
        System.out.println("printOutput");
        LogLevel logLevel = LogLevel.CREATE;
        String event = "5 R22-A 10 30 4";
        String level = "MINOR";
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outContent);
        Robot instance = new Robot();
        String result = instance.formatOutput(logLevel, event, level);

        String expectedResult = "Create 5 R22-A 10 30 4 MINOR";

        assertEquals(expectedResult, result);

        logLevel = LogLevel.ACTIVATE_SHIELD;
        event = "R22-B";
        level = "lixo";
        instance.formatOutput(logLevel, event, level);
        assertEquals(expectedResult, result);

    }

    @Test
    public void testMagic() throws IOException {

    }

}
