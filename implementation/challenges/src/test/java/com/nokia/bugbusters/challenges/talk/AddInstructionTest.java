package com.nokia.bugbusters.challenges.talk;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AddInstructionTest {

    private Computer testComputer = new Computer( 100, 10 );
    private AddInstruction testAddInstruction;
    private int testRegister;

    @Before
    public void setup() {
        testComputer.boot();
        testRegister = 0;
    }

    @After
    public void teardown(){
    }

    @Test
    public void testAddZeroTenTimesIsZero(){
        int targetRegister = 0;
        int addValue = 0;



        AddInstruction testAddInstruction = new AddInstruction( targetRegister , addValue );

        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);

        assertEquals( 0 , testComputer.getRegister( targetRegister).getValue() );

    }

    @Test
    public void testAddNegativeNumber2Times() {
        setTestRegisterValue( 10 );
        testAddInstruction = createAddValueInstruction( -10 );

        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);

        assertTestRegisterValueEquals( -10);
    }



    private void setTestRegisterValue(int value) {
        testComputer.getRegister( testRegister ).setValue( value );
    }

    @Test
    public void testAdd2TimesTen() {
        int targetRegister = 0;
        int addValue = 10;
        AddInstruction testAddInstruction = new AddInstruction( targetRegister , addValue );

        testAddInstruction.execute(testComputer);
        testAddInstruction.execute(testComputer);

        assertTestRegisterValueEquals(20);

    }


    private void assertTestRegisterValueEquals(int expectedValue) {
        assertEquals( expectedValue , testComputer.getRegister(testRegister).getValue() );
    }

    private AddInstruction createAddValueInstruction( int value ) {
        int targetRegister = 0;
        int addValue = value;
        return new AddInstruction( targetRegister , addValue );
    }


}



