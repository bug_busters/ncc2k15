package com.nokia.bugbusters.challenges.talk;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TalkSolverTest{

    private static String lineSeparator = System.lineSeparator();


    @Test
    public void testHaltInstructionOnly() throws IOException {
        String input = "1\n\n100\n";
        String output = runTalkSolver(input);
        assertEquals( "1" + lineSeparator , output );
    }

    @Test
    public void testOneInstructionOnly() throws IOException {
        String input = oneArgPerLine(
                "1"
                , ""
                , "200"
                , "100"
                , "100"
                , "100"
        );

        String expectedOutput = oneArgPerLine(
                "2"
        );


        String output = runTalkSolver(input);
        assertEquals( expectedOutput, output );
    }



    @Test
    public void testExample() throws IOException {
        String input = oneArgPerLine(
            "2"
            , ""
            , "299"
            , "492"
            , "495"
            , "399"
            , "492"
            , "495"
            , "399"
            , "283"
            , "279"
            , "689"
            , "078"
            , "100"
            , "000"
            , "000"
            , "000"
            , ""
            , "100"
        );

        String output = runTalkSolver(input);

        String expectedOutput = oneArgPerLine(
                "16",
                "",
                "1"
        );
        assertEquals( expectedOutput, output );
    }


    @Test
    public void testAddInstruction() throws IOException {
        String input = oneArgPerLine(
                 "1"
                , ""
                , "200"
                , "100"
        );
        String expectedOutput = oneArgPerLine(
                "2"
        );

        String output = runTalkSolver(input);
        assertEquals( expectedOutput , output );
    }

    @Test
    public void testProbTalk9() throws IOException {

        String input = oneArgPerLine(
                 "1"
                , ""
                , "219"
                , "311"
                , "801"
                , "225"
                , "902"
                , "000"
                , "000"
                , "000"
                , "000"
                , "000"
                , "100"
        );

        String expectedOutput = oneArgPerLine(
                "6"
        );

        String output = runTalkSolver(input);
        assertEquals( expectedOutput , output );

    }

    @Test
    public void testProbTalk6() throws IOException {

        String input = oneArgPerLine(
                "1"
                ,""
                ,"205"
                ,"215"
                ,"710"
                ,"225"
                ,"721"
                ,"428"
                ,"839"
                ,"910"
                ,"541"
                ,"554"
                ,"754"
                ,"261"
                ,"965"
                ,"574"
                ,"670"
                ,"672"
                ,"076"
                ,"100"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"000"
                ,"285"
                ,"294"
                ,"798"
                ,"789"
                ,"985"
                ,"053"
                ,"056"
                ,"202"
                ,"249"
                ,"100"
        );

        String expectedOutput = oneArgPerLine(
                "24"
        );

        String output = runTalkSolver(input);
        assertEquals( expectedOutput , output );
    }
    ////////////////////////////////////


    @Test
    public void test3Programs() throws IOException {
        String input = oneArgPerLine(
                "4"
                , ""
                , "000"
                , "100"
                , ""
                , "200"
                , "200"
                , "200"
                , "100"
                , ""
                , "200"
                , "200"
                , "100"
                , ""
                , "200"
                , "100"
        );
        String expectedOutput = oneArgPerLine(
                "2"
                ,""
                ,"4"
                ,""
                ,"3"
                ,""
                ,"2"
        );

        String output = runTalkSolver(input);
        assertEquals( expectedOutput , output );
    }
    ////////////////////////////////////


    private String oneArgPerLine(String... strings) {
       StringBuilder builder = new StringBuilder();
       for(String xpto : strings){
           builder.append(xpto);
           builder.append(System.lineSeparator());
       }
       return builder.toString();    
//return StringUtils.join(Arrays.asList( strings ), "\r\n");
    }


    private String runTalkSolver(String input) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        TalkSolver talkSolver = new TalkSolver(IOUtils.toInputStream(input), new PrintStream(outputStream));
        talkSolver.solve();
        return outputStream.toString();
    }

}
