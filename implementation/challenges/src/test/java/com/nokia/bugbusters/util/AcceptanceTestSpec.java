package com.nokia.bugbusters.util;


import java.nio.file.Paths;

/**
 * Created by Paulo on 11-04-2015.
 */
public class AcceptanceTestSpec {


    private String testInputFilename;
    private String testExpectedOutputFilename;
    private String testName;
    private AcceptanceTestsRuntime testRuntime;

    public static String DEFAULT_INPUT_FILE_SUFFIX= ".in";
    public static String DEFAULT_EXPECTED_OUTPUT_FILE_SUFFIX = ".out";

    private String inputFileSuffix = DEFAULT_INPUT_FILE_SUFFIX;
    private String expectedOutputFileSuffix = DEFAULT_EXPECTED_OUTPUT_FILE_SUFFIX;

    public AcceptanceTestSpec( String testInputFilename){
        this.testInputFilename = testInputFilename;
        this.testExpectedOutputFilename = getOutputFilename( this.testInputFilename );
        this.testName = getTestName( this.testInputFilename );
    }


    private String getTestName(String testInputFilename) {
        return Paths.get( testInputFilename ).getFileName().toString();
    }

    public String getTestInputFilename() {
        return testInputFilename;
    }

    public String getTestExpectedOutputFilename() {
        return testExpectedOutputFilename;
    }

    public String getTestName() {
        return testName;
    }

    public AcceptanceTestsRuntime getTestRuntime() {
        return testRuntime;
    }

    private String getOutputFilename(String inputFilename) {
        return inputFilename.replaceFirst("\\" + inputFileSuffix + "$", expectedOutputFileSuffix);
    }

    public void setTestRuntime(AcceptanceTestsRuntime testRuntime) {
        this.testRuntime = testRuntime;
    }

    public String toString(){
        return new String(""
                + "testName: " + testName + System.lineSeparator()
                + "testInputFilename: " + testInputFilename + System.lineSeparator()
                + "testExpectedOutputFilename: " + testExpectedOutputFilename + System.lineSeparator()
                + "getOutputFilename(): " + getOutputFilename( testInputFilename ) + System.lineSeparator()
                + "testRuntime: " + getTestRuntime().toString() + System.lineSeparator()
        );


    }


}
