package com.nokia.bugbusters.util;

/**
 * Created by user on 10/04/15.
 */
import java.util.Date;

public class Chronometer {

    private long start = 0l;
    private long stop = 0l;
    private String label=null;
    private boolean isCounting;

    public Chronometer( String label) {
        this.startCounting();
        this.label = ( label != null ? label : "Chron."+this.start);
    }
    public Chronometer() {
        this( null);
    }

    public final void startCounting() {
        this.start=new Date().getTime();
        this.isCounting = true;
    }
    public final void stopCounting() {
        this.stop = new Date().getTime();
        this.isCounting = false;
    }
    public long getMilliseconds() {
        if( this.isCounting) {
            this.stop = new Date().getTime();
        }
        return ( this.stop - this.start);
    }

    public String toString() {
        return this.label+" took "+this.getMilliseconds()+" millisecs";
    }

}


