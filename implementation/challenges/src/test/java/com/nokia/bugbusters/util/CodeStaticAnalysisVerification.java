package com.nokia.bugbusters.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by pt103441 on 14-05-2015.
 */

@RunWith(value = Parameterized.class)
public class CodeStaticAnalysisVerification {

    private final String challengeName;

    public CodeStaticAnalysisVerification(String challengeName){
        this.challengeName = challengeName;
    }


    @Test
    public void findBugs(){
        findBugs( this.challengeName );
    }

    @Test
    public void analizeJavaCode(){
        analizeJavaCode(this.challengeName );
    }



    @Parameterized.Parameters(name="{0}")
    public static Iterable<Object[]> generateTestsParameters() {

        //String allChallengesNames[] = getAllChallengesNames();
        String allChallengesNames[] = {"repairin","talk"} ;

        ArrayList<Object[]> allTestsParameters = new ArrayList<>(allChallengesNames.length);


        for (String challengeName: allChallengesNames) {
            Object parameters[] = new Object[1];
            parameters[0] = challengeName;
            allTestsParameters.add(parameters);
        }

        return allTestsParameters;

    }




    public static String[] getAllChallengesNames() {
        File file = new File(TestUtils.getChallengesJavaSourceFilesPackageDir());
        return file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
    }


    static List<String> buildFindBugsCommand( String codeChallengeProblemName) {



        String testCommandWorkingDir = getTestCommandWorkingDir();

        String currPath = TestUtils.getJavaClassFilesRootDirectory();

        List<String> findBugsCommand = new ArrayList<String>();
        String separator = System.getProperty("file.separator");

        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            findBugsCommand.add(currPath + "../../validation/findbugs-3.0.0/bin/findbugs.bat");
        } else {
            findBugsCommand.add(currPath + "../../validation/findbugs-3.0.0/bin/findbugs");
        }
        findBugsCommand.add("-effort:max");
        findBugsCommand.add("-textui");
        findBugsCommand.add("-include");
        findBugsCommand.add(currPath + "../../validation/findbugs-3.0.0/findbugs.xml");
        findBugsCommand.add("-low");
        findBugsCommand.add("-emacs");
        findBugsCommand.add(testCommandWorkingDir  + "/com/nokia/bugbusters/challenges/" + codeChallengeProblemName);
        return findBugsCommand;
    }

    static List<String> buildPmdCommand( String codeChallengeProblemName) {
        String currPath = TestUtils.getJavaClassFilesRootDirectory();

        List<String> pmdCommand = new ArrayList<String>();
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            pmdCommand.add(currPath + "../../validation/pmd-bin-5.2.3/bin/pmd.bat");

        } else {
            pmdCommand.add(currPath + "../../validation/pmd-bin-5.2.3/bin/run.sh");
            pmdCommand.add("pmd"); //linux
        }
        pmdCommand.add("-f");
        pmdCommand.add("text");
        pmdCommand.add("-R");
        pmdCommand.add(currPath + "../../validation/pmd-bin-5.2.3/pmd.xml");
        pmdCommand.add("-version");
        pmdCommand.add("1.7");
        pmdCommand.add("-language");
        pmdCommand.add("java");
        pmdCommand.add("-d");
        pmdCommand.add( TestUtils.getChallengesJavaSourceFilesPackageDir() + "/" + codeChallengeProblemName + "/");
        return pmdCommand;
    }

    static Process executeValidationCommand( List<String> command) {
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.directory(new File( CodeStaticAnalysisVerification.getTestCommandWorkingDir() ));
        Process testRunProcess = null;

        String testCommandWorkingDir =  CodeStaticAnalysisVerification.getTestCommandWorkingDir();
        String cmd = command.toString().replaceAll(",", " ");
        System.out.println("About to run command '" + cmd + "' in directory '" + testCommandWorkingDir);

        try {

            testRunProcess = processBuilder.start();
            testRunProcess.waitFor();
            int retcode = testRunProcess.exitValue();
            if (retcode != 0) {
                System.out.println("Error validating code with command '" + command + "'. command exited with code '" + retcode + "'");
                String stderr = TestUtils.convertStreamToString(testRunProcess.getErrorStream());
                String stdout = TestUtils.convertStreamToString(testRunProcess.getInputStream());
                System.out.println("-- stdout:\n" + stdout);
                System.out.println("--");
                System.out.println("-- stderr:\n" + stderr);
                System.out.println("--");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return testRunProcess;
    }

    public static String getTestCommandWorkingDir() {
        return TestUtils.getJavaClassFilesRootDirectory() + "../classes";
    }

    public static void findBugs(String codeChallengeProblemName) {
        String actualOutput;
        String stderrOutput;

        Process testFindBugsProcess = executeValidationCommand(buildFindBugsCommand(codeChallengeProblemName));

        actualOutput = TestUtils.convertStreamToString(testFindBugsProcess.getInputStream());
        stderrOutput = TestUtils.convertStreamToString(testFindBugsProcess.getErrorStream());

        assertEquals("findbugs stdout is empty", "", actualOutput);
        assertEquals("findbugs stderr is empty", "", stderrOutput);
    }

    public static void analizeJavaCode(String codeChallengeProblemName) {
        String actualOutput;
        String stderrOutput;
        Process testPMDProcess = executeValidationCommand(buildPmdCommand(codeChallengeProblemName));

        actualOutput = TestUtils.convertStreamToString(testPMDProcess.getInputStream());
        stderrOutput = TestUtils.convertStreamToString(testPMDProcess.getErrorStream());

        assertEquals("pmd stdout is empty", "", actualOutput);
        assertEquals("pmd stderr is empty", "", stderrOutput);
    }
}
