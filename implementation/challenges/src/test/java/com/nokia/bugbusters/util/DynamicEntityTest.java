package com.nokia.bugbusters.util;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(value = Parameterized.class)
public class DynamicEntityTest {

    private static AcceptanceTestsRuntime testsRuntime;
    private AcceptanceTestSpec testSpec;

    public DynamicEntityTest( String testName, AcceptanceTestSpec testSpec) {
        this.testSpec = testSpec;
    }

    @Test (timeout=3000)
    public void test() {
        testExecution();
    }

    @Parameterized.Parameters
    public static Iterable<Object[]> generateTestsParameters() {
        return new ArrayList<>();
    }

    protected void testExecution() {
        Process testRunProcess = executeTestCommand(testSpec.getTestRuntime());

        String actualOutput = TestUtils.convertStreamToString(testRunProcess.getInputStream());
        String stderrOutput = TestUtils.convertStreamToString(testRunProcess.getErrorStream());

        String expectedOutput = getFileContentsAsString(testSpec.getTestExpectedOutputFilename());

        assertEquals(testSpec.getTestName() + " stdout", expectedOutput, actualOutput);
        assertEquals(testSpec.getTestName() + " stderr is empty", "", stderrOutput);
    }

    public static Iterable<Object[]> generateTestsParametersForCodeChallengeProblem(String codeChallengeProblemName, String mainClassName) {

        String currPath = TestUtils.getJavaClassFilesRootDirectory();
        String testCommandWorkingDir = CodeStaticAnalysisVerification.getTestCommandWorkingDir();
        String entityTestsResourcesDir = currPath + "../../resources/" + codeChallengeProblemName + "/tests";
        List<String> findBugsCommand = CodeStaticAnalysisVerification.buildFindBugsCommand(codeChallengeProblemName);
        List<String> pmdCommand = CodeStaticAnalysisVerification.buildPmdCommand(codeChallengeProblemName);
        DynamicEntityTest.testsRuntime = new AcceptanceTestsRuntime(
                mainClassName,
                testCommandWorkingDir,
                entityTestsResourcesDir,
                findBugsCommand, pmdCommand
        );

        return getTestFilesAsTestsParameters();

    }

    public static Iterable<Object[]> generateTestsParametersForCodeChallengeProblem(String codeChallengeProblemName) {

        String className = "com.nokia.bugbusters.challenges."
                + codeChallengeProblemName
                + "." + TestUtils.capitalizeFirstLetter(codeChallengeProblemName);

        return generateTestsParametersForCodeChallengeProblem(codeChallengeProblemName, className);

    }


    protected static ArrayList<Object[]> getTestFilesAsTestsParameters() {
        ArrayList<AcceptanceTestSpec> foundTestsSpecs = findTestFilesRecursively(testsRuntime.getTestsRootDir());

        return fillInParametersForAllTests(foundTestsSpecs);
    }

    private String getFileContentsAsString(String filename) {
        failTestIfFileDoesNotExist(filename);
        return TestUtils.convertStreamToString(getInputStreamFromFile(filename));
    }

    private void failTestIfFileDoesNotExist(String filename) {
        if (!new File(filename).exists()) {
            System.err.println("WARN: Check if file '" + filename + "' required to run the test exists and is accessible. Test not executed. Moving on. ");
            fail(filename + " not found. ");
        }
    }

    private Process executeTestCommand(AcceptanceTestsRuntime testRuntime) {

        String inputFilename = this.testSpec.getTestInputFilename();
        failTestIfFileDoesNotExist(inputFilename);
        InputStream testFileInputStream = getInputStreamFromFile(inputFilename);

        ProcessBuilder processBuilder = new ProcessBuilder("java", "-cp", ".", testRuntime.getTestMainClass());
        processBuilder.directory(new File(testRuntime.getTestCommandWorkingDir()));
        Process testRunProcess = null;

        //System.err.println( "Current testSpec: " + testSpec.toString() );
        System.err.println("About to run command 'java " + testRuntime.getTestMainClass() + " < " + testSpec.getTestName() + " ' in directory '" + testRuntime.getTestCommandWorkingDir() + "'");

        try {

            testRunProcess = processBuilder.start();
            copyInputToProcessStdin(testFileInputStream, testRunProcess);
            testFileInputStream.close();
            testRunProcess.waitFor();
            int retcode = testRunProcess.exitValue();
            if (retcode != 0) {
                System.out.println("Error executing test with input file '" + testSpec.getTestName() + "'. java exited with code '" + retcode + "'");
                String stderr = TestUtils.convertStreamToString(testRunProcess.getErrorStream());
                String stdout = TestUtils.convertStreamToString(testRunProcess.getInputStream());
                System.out.println("-- stdout:\n" + stdout);
                System.out.println("--");
                System.out.println("-- stderr:\n" + stderr);
                System.out.println("--");
            }

        } catch (IOException e) {
            System.out.println(testSpec.toString());
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return testRunProcess;
    }

    private void copyInputToProcessStdin(InputStream testFileInput, Process testRunProcess) throws IOException {
        OutputStream testRunStdin = testRunProcess.getOutputStream();
        IOUtils.copy(testFileInput, testRunStdin);
        testRunStdin.close();
    }

    private FileInputStream getInputStreamFromFile(String inputFile) {
        FileInputStream testFileInput = null;
        try {
            testFileInput = new FileInputStream(inputFile);
        } catch (FileNotFoundException e) {
        }
        return testFileInput;
    }

    protected static ArrayList<Object[]> fillInParametersForAllTests(ArrayList<AcceptanceTestSpec> foundTestsSpecs) {
        ArrayList<Object[]> allTestsParameters = new ArrayList<>(foundTestsSpecs.size());
        for (AcceptanceTestSpec acceptanceTestSpec : foundTestsSpecs) {
            acceptanceTestSpec.setTestRuntime(testsRuntime);
            Object parameters[] = new Object[2];
            parameters[0] = acceptanceTestSpec.getTestName();
            parameters[1] = acceptanceTestSpec;
            allTestsParameters.add(parameters);
        }
        return allTestsParameters;
    }

    protected static ArrayList<AcceptanceTestSpec> findTestFilesRecursively(String testsRootDir) {
        Path startingPath = new File(testsRootDir).toPath();
        AcceptanceTestsFinder acceptanceTestsFinder = new AcceptanceTestsFinder();
        try {
            Files.walkFileTree(startingPath, acceptanceTestsFinder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return acceptanceTestsFinder.getFoundTestSpecs();
    }

}

class AcceptanceTestsFinder extends SimpleFileVisitor<Path> {

    private ArrayList<AcceptanceTestSpec> testSpecs = new ArrayList<>();

    @Override
    public FileVisitResult visitFile(Path inputFilePath, BasicFileAttributes attr) {

        if (isNotInputFile(inputFilePath)) {
            return CONTINUE;
        }

        String inputFilename = inputFilePath.toString();
        this.testSpecs.add(new AcceptanceTestSpec(inputFilename));

        return CONTINUE;
    }

    private boolean isNotInputFile(Path inputFilePath) {
        return !inputFilePath.toString().endsWith(AcceptanceTestSpec.DEFAULT_INPUT_FILE_SUFFIX);
    }

    public ArrayList<AcceptanceTestSpec> getFoundTestSpecs() {
        return testSpecs;
    }

}

class AcceptanceTestsRuntime {


    private String testMainClass = "";
    private String testCommandWorkingDir = "";
    private String testsRootDir = "";
    private List<String> testFindBugsToolCommand = null;
    private List<String> testPMDToolCommand = null;

    public AcceptanceTestsRuntime(String testMainClass, String testCommandWorkingDir, String testsRootDir, List<String> findBugsCommand, List<String> pmdCommand) {
        this.testMainClass = testMainClass;
        this.testCommandWorkingDir = testCommandWorkingDir;
        this.testsRootDir = testsRootDir;
        this.testFindBugsToolCommand = findBugsCommand;
        this.testPMDToolCommand = pmdCommand;
    }

    public String getTestMainClass() {
        return testMainClass;
    }

    public String getTestCommandWorkingDir() {
        return testCommandWorkingDir;
    }

    public String getTestsRootDir() {
        return testsRootDir;
    }


    public String toString(){
        return new String(""
                + System.lineSeparator() + "================================================================="
                + System.lineSeparator() + "testMainClass: " + testMainClass
                + System.lineSeparator() + "================================================================="
                + System.lineSeparator() + "testCommandWorkingDir: " + testCommandWorkingDir
                + System.lineSeparator() + "testsRootDir: " + testsRootDir
                + System.lineSeparator() + "testFindBugsToolCommand: " + testFindBugsToolCommand.toString().replaceAll(","," ")
                + System.lineSeparator() + "testPMDToolCommand: " + testPMDToolCommand.toString().replaceAll(","," ")
                + System.lineSeparator() + "================================================================="
        );
    }

}
