package com.nokia.bugbusters.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by pt103441 on 14-05-2015.
 */
public class TestUtils {
    static String convertStreamToString(InputStream testFileExpectedOutputStream) {
        String streamAsString = null;

        try {
            streamAsString = IOUtils.toString(testFileExpectedOutputStream).replaceAll("\r", "");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return streamAsString;
    }

    static String capitalizeFirstLetter(String codeChallengeProblemName) {
        return Character.toUpperCase(codeChallengeProblemName.charAt(0)) + codeChallengeProblemName.substring(1);
    }

    public static String getJavaClassFilesRootDirectory() {
        return ClassLoader.getSystemClassLoader().getResource("").getPath().toString();
    }

    static String getChallengesJavaSourceFilesPackageDir() {
        return getJavaClassFilesRootDirectory() + "../../src/main/java/com/nokia/bugbusters/challenges";
    }
}
