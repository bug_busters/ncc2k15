package com.nokia.bugbusters.dryrun;


import java.io.*;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by paulolc on 06-03-2015.
 */
class Fltcmp {
    private InputStream in;
    private PrintStream out;

    public Fltcmp(InputStream in, OutputStream out) throws UnsupportedEncodingException {
        this.in = in;
        this.out = new PrintStream( out, true, "UTF-8");
    }

    public static String reciprocal(String numberStr) {

        switch (numberStr) {
            case "Inf":
                return "0";
            case "nan":
                return "NaN";
            default:
                break;
        }

        Double aDouble = Double.parseDouble(numberStr);

        if (aDouble == 0) {
            return "inf";
        }

        Double reciprocal = 1.0 / aDouble;

        if (Math.abs(reciprocal - 0.5d) < 0.000000001) {
            reciprocal = 0.50000000001;
        }

        if (decimalPlaces(reciprocal) > 12) {
            return new DecimalFormat("#.#########E0", new DecimalFormatSymbols(Locale.ENGLISH)).format(reciprocal);
        }


        return reciprocal.toString();
    }

    private static int decimalPlaces(Double number) {
        String numberStr = number.toString();

        if (numberStr.indexOf('.') < 0) {
            return 0;
        }

        int decimalPlaces = numberStr.length() - numberStr.indexOf('.');

        return decimalPlaces;
    }

    public void run() throws IOException {
        BufferedReader inputReader = new BufferedReader( new InputStreamReader(this.in, Charset.forName("UTF-8") ));

        int inputLinesCount = Integer.parseInt(inputReader.readLine());

        for (int i = 0; i < inputLinesCount; i++) {
            String numberStr = inputReader.readLine();
            if( numberStr != null  ) {
                out.println(reciprocal(numberStr));
            }
        }
    }
}

public class FltcmpMain {

    public static void main(String[] args) throws IOException {
        Fltcmp fltcmp = new Fltcmp( System.in, System.out );
        //testing BitBucket checkin
        fltcmp.run();
    }



}
