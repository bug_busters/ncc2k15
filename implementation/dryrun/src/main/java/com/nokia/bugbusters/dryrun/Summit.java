package com.nokia.bugbusters.dryrun;

import java.io.*;
import java.nio.charset.Charset;

class SummitSolver{
 // teste ao commit
    public static void run(InputStream testInput, OutputStream testOutput) {
        BufferedReader inputReader = new BufferedReader( new InputStreamReader( testInput, Charset.defaultCharset() ));

        try {
            PrintStream output = new PrintStream( testOutput , true, Charset.defaultCharset().name() );
            String maxsumStr = "";
            String firstLine = inputReader.readLine();
            if( firstLine != null && ! firstLine.equals("")) {
                long inputLinesCount = Long.parseLong(firstLine);
                long maxsum = Long.MIN_VALUE;
                long maxsumBiggestAddend = Long.MIN_VALUE;
                maxsumStr = "";

                for (int i = 0; i < inputLinesCount; i++) {
                    String numberPairStr = inputReader.readLine();
                    if (numberPairStr != null && ! numberPairStr.equals("") ) {
                        String[] numberPair = numberPairStr.split(" ");
                        long addend1 = Long.parseLong(numberPair[0]);
                        long addend2 = Long.parseLong(numberPair[1]);
                        long sum = addend2 + addend1;
                        long biggestAddend = Math.max(addend1, addend2);
                        String sumStr = Math.min(addend1, addend2) + " + " + biggestAddend + " = " + sum;
                        if (sum > maxsum || sum == maxsum && biggestAddend > maxsumBiggestAddend) {
                            maxsum = sum;
                            maxsumStr = sumStr;
                            maxsumBiggestAddend = biggestAddend ;
                        }

                    }
                }
            }
            output.printf("%s%n", maxsumStr);
        } catch (NumberFormatException e) {
        } catch (UnsupportedEncodingException e) {
            System.err.println( "[ERROR] - Error while opening input: '" + e + "'");
        } catch (IOException ioError) {
            System.err.println( "[ERROR] - Error while reading input: '" + ioError + "'");
        }
    }
}



public class Summit {
    public static void main( String args[] ) {
    	// testing multiple checkin
        SummitSolver.run( System.in, System.out);
    }
}
