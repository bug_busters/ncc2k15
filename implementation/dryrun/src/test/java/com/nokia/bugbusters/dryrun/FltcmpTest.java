package com.nokia.bugbusters.dryrun;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FltcmpTest {


    @Test
    public void reciprocalOfInfIsZero(){
        assertEquals("0", Fltcmp.reciprocal("Inf"));
    }

    @Test
    public void reciprocalOfNaNIsNaN(){
        assertEquals("NaN", Fltcmp.reciprocal("nan"));
    }

    @Test
    public void reciprocalOfZeroIsInf(){
        assertEquals("inf", Fltcmp.reciprocal("+0"));
    }

    @Test
    public void reciprocalOfOneIsOne(){
        assertEquals("1.0", Fltcmp.reciprocal("1"));
    }

    @Test
    public void reciprocalOfOneAsFloatIsOne(){
        assertEquals("1.0", Fltcmp.reciprocal("1.0"));
    }

    @Test
    public void reciprocalOfFourHighPrecision(){
        assertEquals("0.25", Fltcmp.reciprocal("4.0000000000000"));
    }

    @Test
    public void reciprocalOfExponent(){
        assertEquals("0.50000000001", Fltcmp.reciprocal("2E0"));
    }

    @Test
    public void reciprocalOfThree(){
        assertEquals("3.333333333E-1", Fltcmp.reciprocal("3"));
    }

    @Test
    public void reciprocalOfFiveHighPrecision(){
        assertEquals("2E-1", Fltcmp.reciprocal("5.0000000000001"));
    }






}