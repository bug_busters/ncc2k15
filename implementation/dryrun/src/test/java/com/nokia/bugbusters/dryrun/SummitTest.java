package com.nokia.bugbusters.dryrun;

import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class SummitTest {

    private InputStream setTestInput(String inputStr) {
        return new ByteArrayInputStream(inputStr.getBytes(Charset.forName("UTF-8")));
    }


    private String summitSolverRunner(String inputStr) {
        OutputStream testOutput = new ByteArrayOutputStream();
        InputStream testInput = setTestInput(inputStr);

        SummitSolver.run(testInput, testOutput);
        String result = testOutput.toString();
        return result.trim();
    }

    @Test
    public void summitSolverAnswersEmptyOutputWithZero() {
        String inputStr = "";
        assertEquals("", summitSolverRunner(inputStr));
    }

    @Test
    public void summitSolverHighestSumOnFirstLine() {
        String inputStr = ""
                + "1\n"
                + "10 10\n";

        assertEquals("10 + 10 = 20", summitSolverRunner(inputStr));
    }

    @Test
    public void summitSolverZeroLines() {
        String inputStr = ""
                + "0\n";

        assertEquals("", summitSolverRunner(inputStr));
    }

    @Test
    public void summitSolverWithTwoMaxSumReturnsTheOneWithHighestParcel() {
        String inputStr = ""
                + "3\n"
                + "1 2\n"
                + "100 10\n"
                + "99 11\n";

        assertEquals("10 + 100 = 110", summitSolverRunner(inputStr));
    }

    @Test
    public void summitSolverLastSumIsHighest() {
        String inputStr = ""
                + "6\n"
                + "-1 -2\n"
                + "-100 -10\n"
                + "100 10\n"
                + "99 11\n"
                + "99 11\n"
                + "-1 111\n";

        assertEquals("-1 + 111 = 110", summitSolverRunner(inputStr));
    }

    @Test
    public void summitSolverFirstSumIsHighest() {
        String inputStr = ""
                + "6\n"
                + "200 19\n"
                + "-100 -10\n"
                + "100 10\n"
                + "99 11\n"
                + "-1 111\n"
                + "99 11\n";

        assertEquals("19 + 200 = 219", summitSolverRunner(inputStr));
    }

    @Test
    public void summitSolverNegativeLineNumber() {
        String inputStr = ""
                + "-6\n"
                + "200 19\n"
                + "-100 -10\n"
                + "100 10\n"
                + "99 11\n"
                + "-1 111\n"
                + "99 11\n";
        assertEquals("", summitSolverRunner(inputStr));

    }

    @Test
    public void summitSolverGarbledInput() {
        String inputStr = "f13k4jçfl23jk4fkl2j43çklj";
        assertEquals("", summitSolverRunner(inputStr));

    }

    @Test
    public void summitSolverZeroSum() {
        String inputStr = ""
                + "1\n"
                + "0 0\n";
        assertEquals("0 + 0 = 0", summitSolverRunner(inputStr));

    }

    @Test
    public void summitSolverZeroSums() {
        String inputStr = ""
                + "3\n"
                + "0 0\n"
                + "0 0\n"
                + "0 0\n";
        assertEquals("0 + 0 = 0", summitSolverRunner(inputStr));

    }

    @Test
    public void summitSolverLessLinesThanSpecified() {
        String inputStr = ""
                + "30\n"
                + "1 2\n"
                + "4 3\n"
                + "5 4\n";
        assertEquals("4 + 5 = 9", summitSolverRunner(inputStr));

    }

    @Test
    public void summitSolverHighestSumIsLongMaxValue() {
        String inputStr = ""
                + "2\n"
                + Long.MAX_VALUE + " 0\n"
                + "4 3\n";
        assertEquals("0 + " + Long.MAX_VALUE + " = " + Long.MAX_VALUE , summitSolverRunner(inputStr));

    }

    @Test
    public void summitSolverHighestSumIsLongMinValue() {
        String inputStr = ""
                + "1\n"
                + Long.MIN_VALUE + " 0\n";
        assertEquals(Long.MIN_VALUE + " + 0 = " + Long.MIN_VALUE , summitSolverRunner(inputStr));

    }

}